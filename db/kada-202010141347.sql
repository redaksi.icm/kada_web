-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for osx10.10 (x86_64)
--
-- Host: localhost    Database: kada
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ap_activity`
--

DROP TABLE IF EXISTS `ap_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sess_id` varchar(32) NOT NULL DEFAULT '0',
  `user_id` varchar(32) NOT NULL DEFAULT '0',
  `fullname` varchar(255) NOT NULL DEFAULT '0',
  `role` varchar(50) NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_activity`
--

LOCK TABLES `ap_activity` WRITE;
/*!40000 ALTER TABLE `ap_activity` DISABLE KEYS */;
INSERT INTO `ap_activity` VALUES (1,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-05 03:15:53','System',NULL,NULL,1,0),(2,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-05 03:15:57','System',NULL,NULL,1,0),(3,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:16:02','System',NULL,NULL,1,0),(4,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','Menu','Menghapus data.','2019-11-05 03:16:28','System',NULL,NULL,1,0),(5,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:16:28','System',NULL,NULL,1,0),(6,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','Menu','Menghapus data.','2019-11-05 03:16:52','System',NULL,NULL,1,0),(7,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:16:52','System',NULL,NULL,1,0),(8,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Profil','Membuat data baru.','2019-11-05 03:16:58','System',NULL,NULL,1,0),(9,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','update','::1','Chrome 78.0.3904.87','Windows 10','Profil','Mengubah data.','2019-11-05 03:18:06','System',NULL,NULL,1,0),(10,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Profil','Membuat data baru.','2019-11-05 03:18:07','System',NULL,NULL,1,0),(11,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:18:18','System',NULL,NULL,1,0),(12,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:18:37','System',NULL,NULL,1,0),(13,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:18:37','System',NULL,NULL,1,0),(14,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:19:09','System',NULL,NULL,1,0),(15,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:19:09','System',NULL,NULL,1,0),(16,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:19:25','System',NULL,NULL,1,0),(17,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:19:25','System',NULL,NULL,1,0),(18,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:19:51','System',NULL,NULL,1,0),(19,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:19:51','System',NULL,NULL,1,0),(20,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:20:15','System',NULL,NULL,1,0),(21,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:20:16','System',NULL,NULL,1,0),(22,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:20:38','System',NULL,NULL,1,0),(23,'tg0a0d7hkvu8b40he1frfhn2d6csb41m','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:20:38','System',NULL,NULL,1,0),(24,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:20:54','System',NULL,NULL,1,0),(25,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:20:54','System',NULL,NULL,1,0),(26,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Grup','Membuat data baru.','2019-11-05 03:20:57','System',NULL,NULL,1,0),(27,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','Grup','Menghapus data.','2019-11-05 03:21:03','System',NULL,NULL,1,0),(28,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Grup','Membuat data baru.','2019-11-05 03:21:03','System',NULL,NULL,1,0),(29,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','update','::1','Chrome 78.0.3904.87','Windows 10','Grup','Mengubah data.','2019-11-05 03:21:27','System',NULL,NULL,1,0),(30,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Grup','Membuat data baru.','2019-11-05 03:21:27','System',NULL,NULL,1,0),(31,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:21:44','System',NULL,NULL,1,0),(32,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:21:52','System',NULL,NULL,1,0),(33,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Menu','Membuat data baru.','2019-11-05 03:21:52','System',NULL,NULL,1,0),(34,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Grup','Membuat data baru.','2019-11-05 03:21:55','System',NULL,NULL,1,0),(35,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','update','::1','Chrome 78.0.3904.87','Windows 10','Grup','Mengubah data.','2019-11-05 03:22:02','System',NULL,NULL,1,0),(36,'a97ndriide6oe46c0jehba3plp778p6e','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Grup','Membuat data baru.','2019-11-05 03:22:02','System',NULL,NULL,1,0),(37,'k8fodb7cv5v0jk9qg708mpap1ged2m6f','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-06 01:29:35','System',NULL,NULL,1,0),(38,'k8fodb7cv5v0jk9qg708mpap1ged2m6f','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-06 01:29:39','System',NULL,NULL,1,0),(39,'k8fodb7cv5v0jk9qg708mpap1ged2m6f','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:29:45','System',NULL,NULL,1,0),(40,'k8fodb7cv5v0jk9qg708mpap1ged2m6f','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:31:44','System',NULL,NULL,1,0),(41,'k8fodb7cv5v0jk9qg708mpap1ged2m6f','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:31:53','System',NULL,NULL,1,0),(42,'k8fodb7cv5v0jk9qg708mpap1ged2m6f','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:32:28','System',NULL,NULL,1,0),(43,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:33:56','System',NULL,NULL,1,0),(44,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:33:57','System',NULL,NULL,1,0),(45,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:34:22','System',NULL,NULL,1,0),(46,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:34:25','System',NULL,NULL,1,0),(47,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:34:29','System',NULL,NULL,1,0),(48,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:34:30','System',NULL,NULL,1,0),(49,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 01:34:32','System',NULL,NULL,1,0),(50,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-06 01:36:51','System',NULL,NULL,1,0),(51,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-06 01:36:57','System',NULL,NULL,1,0),(52,'qmvb4im3algcjuti6prf4s6cv17dkqnt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-06 01:36:57','System',NULL,NULL,1,0),(53,'r44n8te2requvq5gfgifpn0tat332it4','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-06 02:11:23','System',NULL,NULL,1,0),(54,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-06 02:11:27','System',NULL,NULL,1,0),(55,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:11:33','System',NULL,NULL,1,0),(56,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:12:07','System',NULL,NULL,1,0),(57,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:12:07','System',NULL,NULL,1,0),(58,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:13:29','System',NULL,NULL,1,0),(59,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:13:29','System',NULL,NULL,1,0),(60,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:13:47','System',NULL,NULL,1,0),(61,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:13:47','System',NULL,NULL,1,0),(62,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:14:16','System',NULL,NULL,1,0),(63,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:14:16','System',NULL,NULL,1,0),(64,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:14:48','System',NULL,NULL,1,0),(65,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:14:49','System',NULL,NULL,1,0),(66,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','update','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Mengubah data.','2019-11-06 02:15:11','System',NULL,NULL,1,0),(67,'r44n8te2requvq5gfgifpn0tat332it4','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:15:11','System',NULL,NULL,1,0),(68,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Menghapus data.','2019-11-06 02:21:39','System',NULL,NULL,1,0),(69,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:21:40','System',NULL,NULL,1,0),(70,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Menghapus data.','2019-11-06 02:21:43','System',NULL,NULL,1,0),(71,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:21:43','System',NULL,NULL,1,0),(72,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Menghapus data.','2019-11-06 02:21:47','System',NULL,NULL,1,0),(73,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:21:47','System',NULL,NULL,1,0),(74,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Menghapus data.','2019-11-06 02:21:50','System',NULL,NULL,1,0),(75,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:21:50','System',NULL,NULL,1,0),(76,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Menghapus data.','2019-11-06 02:21:54','System',NULL,NULL,1,0),(77,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:21:54','System',NULL,NULL,1,0),(78,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:22:10','System',NULL,NULL,1,0),(79,'goc9ndg61eo0hvh128uub9bogo9c076j','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-06 02:24:34','System',NULL,NULL,1,0),(80,'6qofaiqr6uacbm8c2os96t3sm0sk94bu','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-06 05:51:37','System',NULL,NULL,1,0),(81,'lvg0t35t3ikolgbbm339d64qli11dspv','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-11 02:20:56','System',NULL,NULL,1,0),(82,'lvg0t35t3ikolgbbm339d64qli11dspv','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-11 02:21:00','System',NULL,NULL,1,0),(83,'js3ect6uhc5o6p98sjks6glfaun7cqmq','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-11 05:58:21','System',NULL,NULL,1,0),(84,'js3ect6uhc5o6p98sjks6glfaun7cqmq','b785a32d7e8e37635823a27fbb13cdf9','User Farmasi','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-11 05:58:25','System',NULL,NULL,1,0),(85,'js3ect6uhc5o6p98sjks6glfaun7cqmq','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-11 05:58:25','System',NULL,NULL,1,0),(86,'js3ect6uhc5o6p98sjks6glfaun7cqmq','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-11 05:58:29','System',NULL,NULL,1,0),(87,'js3ect6uhc5o6p98sjks6glfaun7cqmq','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-11 05:58:38','System',NULL,NULL,1,0),(88,'js3ect6uhc5o6p98sjks6glfaun7cqmq','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-11 05:58:42','System',NULL,NULL,1,0),(89,'js3ect6uhc5o6p98sjks6glfaun7cqmq','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-11 05:58:44','System',NULL,NULL,1,0),(90,'js3ect6uhc5o6p98sjks6glfaun7cqmq','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-11 05:59:51','System',NULL,NULL,1,0),(91,'js3ect6uhc5o6p98sjks6glfaun7cqmq','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','KKDA','Membuat data baru.','2019-11-11 05:59:51','System',NULL,NULL,1,0),(92,'h8sl2sdhltggqp7ekfvdes2076n3d2b3','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-12 01:26:44','System',NULL,NULL,1,0),(93,'h8sl2sdhltggqp7ekfvdes2076n3d2b3','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-12 01:27:09','System',NULL,NULL,1,0),(94,'p7ggan4b2qm3d1lomrj81234dslcam4r','-','-','access','::1','Chrome 78.0.3904.87','Windows 10','Login','Mengakses aplikasi.','2019-11-12 02:00:51','System',NULL,NULL,1,0),(95,'p7ggan4b2qm3d1lomrj81234dslcam4r','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 78.0.3904.87','Windows 10','Login','Masuk ke sistem','2019-11-12 02:00:56','System',NULL,NULL,1,0),(96,'p7ggan4b2qm3d1lomrj81234dslcam4r','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:05:11','System',NULL,NULL,1,0),(97,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:27','System',NULL,NULL,1,0),(98,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:28','System',NULL,NULL,1,0),(99,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:35','System',NULL,NULL,1,0),(100,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:35','System',NULL,NULL,1,0),(101,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:43','System',NULL,NULL,1,0),(102,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:43','System',NULL,NULL,1,0),(103,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:50','System',NULL,NULL,1,0),(104,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:56','System',NULL,NULL,1,0),(105,'d7blt7b4v481id4rbhid0gmunkiklpt9','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Inflasi','Membuat data baru.','2019-11-12 02:06:56','System',NULL,NULL,1,0),(106,'b1j5k4c37o8bhv1j350c3o0aodtkqf0n','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Kemiskinan','Membuat data baru.','2019-11-12 02:24:57','System',NULL,NULL,1,0),(107,'b1j5k4c37o8bhv1j350c3o0aodtkqf0n','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 78.0.3904.87','Windows 10','Kemiskinan','Membuat data baru.','2019-11-12 02:25:02','System',NULL,NULL,1,0),(108,'b1j5k4c37o8bhv1j350c3o0aodtkqf0n','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 78.0.3904.87','Windows 10','Kemiskinan','Membuat data baru.','2019-11-12 02:25:02','System',NULL,NULL,1,0),(109,'vsao1kqrmk170vsqcjs1ourank5k8q40','-','-','access','::1','Chrome 85.0.4183.121','Mac OS X','Login','Mengakses aplikasi.','2020-10-14 04:42:21','System',NULL,NULL,1,0),(110,'vsao1kqrmk170vsqcjs1ourank5k8q40','-','-','access','::1','Chrome 85.0.4183.121','Mac OS X','Login','Mengakses aplikasi.','2020-10-14 04:42:22','System',NULL,NULL,1,0),(111,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 85.0.4183.121','Mac OS X','Login','Masuk ke sistem','2020-10-14 04:42:58','System',NULL,NULL,1,0),(112,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Profil','Membuat data baru.','2020-10-14 04:43:03','System',NULL,NULL,1,0),(113,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','update','::1','Chrome 85.0.4183.121','Mac OS X','Profil','Mengubah data.','2020-10-14 04:43:43','System',NULL,NULL,1,0),(114,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Profil','Membuat data baru.','2020-10-14 04:43:43','System',NULL,NULL,1,0),(115,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Parameter','Membuat data baru.','2020-10-14 04:43:49','System',NULL,NULL,1,0),(116,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Parameter','Membuat data baru.','2020-10-14 04:44:03','System',NULL,NULL,1,0),(117,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Menu','Membuat data baru.','2020-10-14 04:44:05','System',NULL,NULL,1,0),(118,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','delete','::1','Chrome 85.0.4183.121','Mac OS X','Menu','Menghapus data.','2020-10-14 04:44:14','System',NULL,NULL,1,0),(119,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Menu','Membuat data baru.','2020-10-14 04:44:15','System',NULL,NULL,1,0),(120,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Inflasi','Membuat data baru.','2020-10-14 04:44:18','System',NULL,NULL,1,0),(121,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','KKDA','Membuat data baru.','2020-10-14 04:44:19','System',NULL,NULL,1,0),(122,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Menu','Membuat data baru.','2020-10-14 04:44:27','System',NULL,NULL,1,0),(123,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','update','::1','Chrome 85.0.4183.121','Mac OS X','Menu','Mengubah data.','2020-10-14 04:44:40','System',NULL,NULL,1,0),(124,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Menu','Membuat data baru.','2020-10-14 04:44:40','System',NULL,NULL,1,0),(125,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Menu','Membuat data baru.','2020-10-14 04:45:17','System',NULL,NULL,1,0),(126,'vsao1kqrmk170vsqcjs1ourank5k8q40','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 04:45:19','System',NULL,NULL,1,0),(127,'u92akm0ekp7cujj6phadbnppn2sr9pu6','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 05:32:16','System',NULL,NULL,1,0),(128,'u92akm0ekp7cujj6phadbnppn2sr9pu6','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 05:32:19','System',NULL,NULL,1,0),(129,'u92akm0ekp7cujj6phadbnppn2sr9pu6','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 05:33:01','System',NULL,NULL,1,0),(130,'u92akm0ekp7cujj6phadbnppn2sr9pu6','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 05:33:03','System',NULL,NULL,1,0),(131,'u92akm0ekp7cujj6phadbnppn2sr9pu6','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 05:33:46','System',NULL,NULL,1,0),(132,'u92akm0ekp7cujj6phadbnppn2sr9pu6','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','update','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Mengubah data.','2020-10-14 05:36:25','System',NULL,NULL,1,0),(133,'u92akm0ekp7cujj6phadbnppn2sr9pu6','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 05:36:25','System',NULL,NULL,1,0),(134,'062ilrqcrhdi5erads18nnq21mga3ies','-','-','access','::1','Chrome 85.0.4183.121','Mac OS X','Login','Mengakses aplikasi.','2020-10-14 06:04:20','System',NULL,NULL,1,0),(135,'062ilrqcrhdi5erads18nnq21mga3ies','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 85.0.4183.121','Mac OS X','Login','Masuk ke sistem','2020-10-14 06:04:24','System',NULL,NULL,1,0),(136,'062ilrqcrhdi5erads18nnq21mga3ies','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','logout','::1','Chrome 85.0.4183.121','Mac OS X','Logout','Keluar dari sistem','2020-10-14 06:08:52','System',NULL,NULL,1,0),(137,'l1klsbi8mndtajir1o1qtdkn07uoitkp','-','-','access','::1','Chrome 85.0.4183.121','Mac OS X','Login','Mengakses aplikasi.','2020-10-14 06:08:52','System',NULL,NULL,1,0),(138,'l1klsbi8mndtajir1o1qtdkn07uoitkp','-','-','access','::1','Chrome 85.0.4183.121','Mac OS X','Login','Mengakses aplikasi.','2020-10-14 06:09:11','System',NULL,NULL,1,0),(139,'l1klsbi8mndtajir1o1qtdkn07uoitkp','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','login','::1','Chrome 85.0.4183.121','Mac OS X','Login','Masuk ke sistem','2020-10-14 06:09:15','System',NULL,NULL,1,0),(140,'l1klsbi8mndtajir1o1qtdkn07uoitkp','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 06:10:17','System',NULL,NULL,1,0),(141,'l1klsbi8mndtajir1o1qtdkn07uoitkp','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 06:10:22','System',NULL,NULL,1,0),(142,'l1klsbi8mndtajir1o1qtdkn07uoitkp','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 06:10:25','System',NULL,NULL,1,0),(143,'l1klsbi8mndtajir1o1qtdkn07uoitkp','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','create','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 06:11:55','System',NULL,NULL,1,0),(144,'l1klsbi8mndtajir1o1qtdkn07uoitkp','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 06:11:55','System',NULL,NULL,1,0),(145,'835qn14lp4dvk1bgoa3ra3hs6e0940lt','17c4520f6cfd1ab53d8745e84681eb49','Super Administrator','read','::1','Chrome 85.0.4183.121','Mac OS X','Statistik','Membuat data baru.','2020-10-14 06:37:52','System',NULL,NULL,1,0);
/*!40000 ALTER TABLE `ap_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_group`
--

DROP TABLE IF EXISTS `ap_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_group` (
  `id` varchar(32) NOT NULL,
  `group` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_group`
--

LOCK TABLES `ap_group` WRITE;
/*!40000 ALTER TABLE `ap_group` DISABLE KEYS */;
INSERT INTO `ap_group` VALUES ('c4ca4238a0b923820dcc509a6f75849b','superadmin','Root access.','2019-07-02 14:59:26','System','2019-07-17 23:09:54','Super Administrator',1,0);
/*!40000 ALTER TABLE `ap_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_group_menu`
--

DROP TABLE IF EXISTS `ap_group_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_group_menu` (
  `menu_id` varchar(50) NOT NULL,
  `group_id` varchar(32) NOT NULL DEFAULT '',
  `created` timestamp(1) NOT NULL DEFAULT CURRENT_TIMESTAMP(1),
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_group_menu`
--

LOCK TABLES `ap_group_menu` WRITE;
/*!40000 ALTER TABLE `ap_group_menu` DISABLE KEYS */;
INSERT INTO `ap_group_menu` VALUES ('00','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('01','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('01','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('02','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('03','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('04','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('05','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('06','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('07','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('08','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('22.01','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('22.02','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('23.01.01','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('23.01.02','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('90','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('90','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('97','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('97','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('97.01','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('97.01','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('97.02','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('97.02','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('98','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('98','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('98.01','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('98.01','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('98.02','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('98.02','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('99','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('99','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('99.01','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('99.01','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0),('99.02','aa2fe6c3f3e60c0c4003953997f64fb2','2019-08-06 20:53:59.0','Super Administrator',NULL,NULL,1,0),('99.02','c4ca4238a0b923820dcc509a6f75849b','2019-11-05 03:21:58.0','Super Administrator',NULL,NULL,1,0);
/*!40000 ALTER TABLE `ap_group_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_group_role`
--

DROP TABLE IF EXISTS `ap_group_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_group_role` (
  `group_id` varchar(255) NOT NULL,
  `menu_id` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`menu_id`,`role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_group_role`
--

LOCK TABLES `ap_group_role` WRITE;
/*!40000 ALTER TABLE `ap_group_role` DISABLE KEYS */;
INSERT INTO `ap_group_role` VALUES ('aa2fe6c3f3e60c0c4003953997f64fb2','01','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.01','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.01','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.01','export_pdf','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.01','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.01','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.02','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.02','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.02','export_pdf','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.02','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','22.02','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.01','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.01','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.01','export_pdf','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.01','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.01','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.02','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.02','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.02','export_pdf','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.02','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','23.01.02','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','90','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.01','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.01','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.01','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.01','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.02','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.02','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.02','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','97.02','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.01','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.01','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.01','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.01','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.02','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.02','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.02','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','98.02','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','99','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','99.01','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','99.01','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','99.02','create','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','99.02','delete','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','99.02','read','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('aa2fe6c3f3e60c0c4003953997f64fb2','99.02','update','2019-08-06 20:53:59','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','00','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','01','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','02','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','02','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','02','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','02','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','03','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','03','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','03','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','03','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','04','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','04','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','04','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','04','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','05','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','05','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','05','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','05','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','06','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','06','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','06','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','06','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','07','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','07','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','07','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','07','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','08','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','08','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','08','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','08','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.01','create','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.01','delete','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.01','export_pdf','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.01','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.01','update','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.02','create','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.02','delete','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.02','export_pdf','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.02','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','22.02','update','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.01','create','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.01','delete','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.01','export_pdf','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.01','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.01','update','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.02','create','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.02','delete','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.02','export_pdf','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.02','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.02','update','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.03','create','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.03','delete','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.03','export_pdf','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.03','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.03','update','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.04','create','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.04','delete','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.04','export_pdf','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.04','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','23.01.04','update','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','46','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','46.01','export_pdf','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','46.01','read','2019-08-14 23:40:41','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','90','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','91','read','2019-07-21 07:12:05','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','92','read','2019-07-21 07:13:39','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.01','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.01','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.01','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.01','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.02','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.02','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.02','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','97.02','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.01','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.01','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.01','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.01','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.02','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.02','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.02','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','98.02','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','99','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','99.01','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','99.01','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','99.02','create','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','99.02','delete','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','99.02','read','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0),('c4ca4238a0b923820dcc509a6f75849b','99.02','update','2019-11-05 03:21:58','Super Administrator',NULL,NULL,1,0);
/*!40000 ALTER TABLE `ap_group_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_menu`
--

DROP TABLE IF EXISTS `ap_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_menu` (
  `id` varchar(50) NOT NULL,
  `parent` varchar(50) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `menu` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_menu`
--

LOCK TABLES `ap_menu` WRITE;
/*!40000 ALTER TABLE `ap_menu` DISABLE KEYS */;
INSERT INTO `ap_menu` VALUES ('00','',3,'MENU UTAMA','#','#','#','2019-11-05 03:21:52','Super Administrator',NULL,NULL,1,0),('01','',2,'Dashboard','ap_dashboard','dashboard','index','2019-07-04 04:45:32','System','2019-08-18 22:54:22','Super Administrator',1,0),('02','',2,'Statistik','dt_statistik','files-o','index','2019-11-05 03:18:37','Super Administrator','2020-10-14 04:44:40','Super Administrator',1,0),('03','',2,'Inflasi','dt_inflasi','area-chart','index','2019-11-05 03:19:09','Super Administrator',NULL,NULL,1,0),('04','',2,'Kemiskinan','dt_kemiskinan','money','index','2019-11-05 03:19:25','Super Administrator',NULL,NULL,1,0),('05','',2,'Pengangguran','dt_pengangguran','briefcase','index','2019-11-05 03:19:50','Super Administrator',NULL,NULL,1,0),('06','',2,'IPM','dt_ipm','user-secret','index','2019-11-05 03:20:15','Super Administrator',NULL,NULL,1,0),('07','',2,'Pertumbuhan Ekonomi','dt_ekonomi','bar-chart','index','2019-11-05 03:20:38','Super Administrator',NULL,NULL,1,0),('08','',2,'Penduduk','dt_penduduk','users','index','2019-11-05 03:20:54','Super Administrator',NULL,NULL,1,0),('90','',3,'PENGATURAN','#','#','#','2019-07-04 22:35:00','System','2019-07-16 00:54:13','Super Administrator',1,0),('97','',1,'Menu','#','list','#','2019-07-04 22:35:21','System','2019-07-21 20:03:48','Super Administrator',1,0),('97.01','97',2,'Role','ap_role','user-secret','index','2019-07-04 22:38:13','System','2019-08-06 20:51:46','Super Administrator',1,0),('97.02','97',2,'Menu','ap_menu','list','index','2019-07-04 22:38:26','System','2019-08-06 20:51:36','Super Administrator',1,0),('98','',1,'Autentikasi','#','id-card-o','#','2019-07-16 00:15:08','Super Administrator','2019-07-21 20:04:01','Super Administrator',1,0),('98.01','98',2,'Grup','ap_group','users','index','2019-07-16 00:22:03','Super Administrator','2019-08-06 20:51:27','Super Administrator',1,0),('98.02','98',2,'Pengguna','ap_user','user','index','2019-07-16 21:12:26','Super Administrator','2019-08-06 20:51:19','Super Administrator',1,0),('99','',1,'Aplikasi','','clone','','2019-07-21 07:10:27','Super Administrator','2019-07-21 20:04:09','Super Administrator',1,0),('99.01','99',2,'Profil','ap_profile','university','index','2019-07-21 07:11:08','Super Administrator','2019-08-06 20:50:43','Super Administrator',1,0),('99.02','99',2,'Parameter','ap_parameter','list','index','2019-07-21 07:11:43','Super Administrator','2019-08-06 20:50:56','Super Administrator',1,0);
/*!40000 ALTER TABLE `ap_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_menu_role`
--

DROP TABLE IF EXISTS `ap_menu_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_menu_role` (
  `menu_id` varchar(50) NOT NULL,
  `role_id` varchar(50) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_menu_role`
--

LOCK TABLES `ap_menu_role` WRITE;
/*!40000 ALTER TABLE `ap_menu_role` DISABLE KEYS */;
INSERT INTO `ap_menu_role` VALUES ('01','ecae13117d6f0584c25a9da6c8f8415e','2019-08-18 22:54:22','Super Administrator',NULL,NULL,1,0),('02','099af53f601532dbd31e0ea99ffdeb64','2020-10-14 04:44:40','Super Administrator',NULL,NULL,1,0),('02','3ac340832f29c11538fbe2d6f75e8bcc','2020-10-14 04:44:40','Super Administrator',NULL,NULL,1,0),('02','76ea0bebb3c22822b4f0dd9c9fd021c5','2020-10-14 04:44:40','Super Administrator',NULL,NULL,1,0),('02','ecae13117d6f0584c25a9da6c8f8415e','2020-10-14 04:44:40','Super Administrator',NULL,NULL,1,0),('03','099af53f601532dbd31e0ea99ffdeb64','2019-11-05 03:19:09','Super Administrator',NULL,NULL,1,0),('03','3ac340832f29c11538fbe2d6f75e8bcc','2019-11-05 03:19:09','Super Administrator',NULL,NULL,1,0),('03','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-11-05 03:19:09','Super Administrator',NULL,NULL,1,0),('03','ecae13117d6f0584c25a9da6c8f8415e','2019-11-05 03:19:09','Super Administrator',NULL,NULL,1,0),('04','099af53f601532dbd31e0ea99ffdeb64','2019-11-05 03:19:25','Super Administrator',NULL,NULL,1,0),('04','3ac340832f29c11538fbe2d6f75e8bcc','2019-11-05 03:19:25','Super Administrator',NULL,NULL,1,0),('04','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-11-05 03:19:25','Super Administrator',NULL,NULL,1,0),('04','ecae13117d6f0584c25a9da6c8f8415e','2019-11-05 03:19:25','Super Administrator',NULL,NULL,1,0),('05','099af53f601532dbd31e0ea99ffdeb64','2019-11-05 03:19:50','Super Administrator',NULL,NULL,1,0),('05','3ac340832f29c11538fbe2d6f75e8bcc','2019-11-05 03:19:50','Super Administrator',NULL,NULL,1,0),('05','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-11-05 03:19:50','Super Administrator',NULL,NULL,1,0),('05','ecae13117d6f0584c25a9da6c8f8415e','2019-11-05 03:19:50','Super Administrator',NULL,NULL,1,0),('06','099af53f601532dbd31e0ea99ffdeb64','2019-11-05 03:20:15','Super Administrator',NULL,NULL,1,0),('06','3ac340832f29c11538fbe2d6f75e8bcc','2019-11-05 03:20:15','Super Administrator',NULL,NULL,1,0),('06','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-11-05 03:20:15','Super Administrator',NULL,NULL,1,0),('06','ecae13117d6f0584c25a9da6c8f8415e','2019-11-05 03:20:15','Super Administrator',NULL,NULL,1,0),('07','099af53f601532dbd31e0ea99ffdeb64','2019-11-05 03:20:38','Super Administrator',NULL,NULL,1,0),('07','3ac340832f29c11538fbe2d6f75e8bcc','2019-11-05 03:20:38','Super Administrator',NULL,NULL,1,0),('07','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-11-05 03:20:38','Super Administrator',NULL,NULL,1,0),('07','ecae13117d6f0584c25a9da6c8f8415e','2019-11-05 03:20:38','Super Administrator',NULL,NULL,1,0),('08','099af53f601532dbd31e0ea99ffdeb64','2019-11-05 03:20:54','Super Administrator',NULL,NULL,1,0),('08','3ac340832f29c11538fbe2d6f75e8bcc','2019-11-05 03:20:54','Super Administrator',NULL,NULL,1,0),('08','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-11-05 03:20:54','Super Administrator',NULL,NULL,1,0),('08','ecae13117d6f0584c25a9da6c8f8415e','2019-11-05 03:20:54','Super Administrator',NULL,NULL,1,0),('12.030','099af53f601532dbd31e0ea99ffdeb64','2019-10-14 22:50:29','Super Administrator',NULL,NULL,1,0),('12.030','3ac340832f29c11538fbe2d6f75e8bcc','2019-10-14 22:50:29','Super Administrator',NULL,NULL,1,0),('12.030','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-10-14 22:50:29','Super Administrator',NULL,NULL,1,0),('12.030','ecae13117d6f0584c25a9da6c8f8415e','2019-10-14 22:50:29','Super Administrator',NULL,NULL,1,0),('97.01','099af53f601532dbd31e0ea99ffdeb64','2019-08-06 20:51:46','Super Administrator',NULL,NULL,1,0),('97.01','3ac340832f29c11538fbe2d6f75e8bcc','2019-08-06 20:51:46','Super Administrator',NULL,NULL,1,0),('97.01','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-08-06 20:51:46','Super Administrator',NULL,NULL,1,0),('97.01','ecae13117d6f0584c25a9da6c8f8415e','2019-08-06 20:51:46','Super Administrator',NULL,NULL,1,0),('97.02','099af53f601532dbd31e0ea99ffdeb64','2019-08-06 20:51:36','Super Administrator',NULL,NULL,1,0),('97.02','3ac340832f29c11538fbe2d6f75e8bcc','2019-08-06 20:51:36','Super Administrator',NULL,NULL,1,0),('97.02','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-08-06 20:51:36','Super Administrator',NULL,NULL,1,0),('97.02','ecae13117d6f0584c25a9da6c8f8415e','2019-08-06 20:51:36','Super Administrator',NULL,NULL,1,0),('98.01','099af53f601532dbd31e0ea99ffdeb64','2019-08-06 20:51:27','Super Administrator',NULL,NULL,1,0),('98.01','3ac340832f29c11538fbe2d6f75e8bcc','2019-08-06 20:51:27','Super Administrator',NULL,NULL,1,0),('98.01','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-08-06 20:51:27','Super Administrator',NULL,NULL,1,0),('98.01','ecae13117d6f0584c25a9da6c8f8415e','2019-08-06 20:51:27','Super Administrator',NULL,NULL,1,0),('98.02','099af53f601532dbd31e0ea99ffdeb64','2019-08-06 20:51:19','Super Administrator',NULL,NULL,1,0),('98.02','3ac340832f29c11538fbe2d6f75e8bcc','2019-08-06 20:51:19','Super Administrator',NULL,NULL,1,0),('98.02','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-08-06 20:51:19','Super Administrator',NULL,NULL,1,0),('98.02','ecae13117d6f0584c25a9da6c8f8415e','2019-08-06 20:51:19','Super Administrator',NULL,NULL,1,0),('99.01','3ac340832f29c11538fbe2d6f75e8bcc','2019-08-06 20:50:43','Super Administrator',NULL,NULL,1,0),('99.01','ecae13117d6f0584c25a9da6c8f8415e','2019-08-06 20:50:43','Super Administrator',NULL,NULL,1,0),('99.02','099af53f601532dbd31e0ea99ffdeb64','2019-08-06 20:50:56','Super Administrator',NULL,NULL,1,0),('99.02','3ac340832f29c11538fbe2d6f75e8bcc','2019-08-06 20:50:56','Super Administrator',NULL,NULL,1,0),('99.02','76ea0bebb3c22822b4f0dd9c9fd021c5','2019-08-06 20:50:56','Super Administrator',NULL,NULL,1,0),('99.02','ecae13117d6f0584c25a9da6c8f8415e','2019-08-06 20:50:56','Super Administrator',NULL,NULL,1,0);
/*!40000 ALTER TABLE `ap_menu_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_parameter`
--

DROP TABLE IF EXISTS `ap_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_parameter` (
  `id` varchar(32) NOT NULL,
  `parameter` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_parameter`
--

LOCK TABLES `ap_parameter` WRITE;
/*!40000 ALTER TABLE `ap_parameter` DISABLE KEYS */;
INSERT INTO `ap_parameter` VALUES ('419a9a369aeafb637b7bb765999a2fed','NPWP','71.228.523.8-523.000','2019-10-24 00:52:09','Super Administrator',NULL,NULL,1,0),('7b31741a16711bde6dcfe2311971c21e','NIP_KEPALA','19720717 200312 1 004','2019-07-31 20:05:56','Super Administrator',NULL,NULL,1,0),('7b90c7a4b8980225ee91947a75b6038c','NAMA_KEPALA','dr. Brantas Prayoga, M.Sc.','2019-07-21 07:17:46','Super Administrator','2019-07-31 20:06:28','Super Administrator',1,0);
/*!40000 ALTER TABLE `ap_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_profile`
--

DROP TABLE IF EXISTS `ap_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_profile` (
  `id` varchar(50) NOT NULL,
  `aplikasi` varchar(255) NOT NULL,
  `singkatan` varchar(255) NOT NULL,
  `opd` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL DEFAULT '0',
  `pemda` varchar(255) NOT NULL DEFAULT '0',
  `kab_kota` varchar(255) NOT NULL DEFAULT '0',
  `kecamatan` varchar(255) NOT NULL DEFAULT '0',
  `alamat` varchar(255) NOT NULL,
  `kodepos` varchar(255) NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `about` text NOT NULL,
  `copyright` varchar(255) NOT NULL,
  `logo` text NOT NULL,
  `version` varchar(20) NOT NULL,
  `start_year` int(4) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_profile`
--

LOCK TABLES `ap_profile` WRITE;
/*!40000 ALTER TABLE `ap_profile` DISABLE KEYS */;
INSERT INTO `ap_profile` VALUES ('4feedc31c1191ad2370ac9b180d4fa0a','Kabupaten Alor Dalam Angka','KADA','Dinas Komunikasi dan Informasi','Nusa Tenggaran Timur','Kabupaten','Alor','Alor','-','0','-','-','pemkab@alorkab.go.id','','','logo.png','0.1.0',2019,'2019-07-02 00:41:39','System','2020-10-14 04:43:43','Super Administrator',0,0);
/*!40000 ALTER TABLE `ap_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_role`
--

DROP TABLE IF EXISTS `ap_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_role` (
  `id` varchar(32) NOT NULL,
  `role` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_role`
--

LOCK TABLES `ap_role` WRITE;
/*!40000 ALTER TABLE `ap_role` DISABLE KEYS */;
INSERT INTO `ap_role` VALUES ('099af53f601532dbd31e0ea99ffdeb64','delete','Menghapus data.','2019-07-03 01:05:46','System','2019-07-10 20:46:18',NULL,1,0),('0aaa87422396fdd678498793b6d5250e','disable','Menonaktifkan data.','2019-07-01 20:44:47','System',NULL,NULL,1,1),('1d35969c41452fe2b721248a96af64d9','export_pdf','Ekspor data berupa file PDF.','2019-07-28 23:44:56','Super Administrator','2019-08-18 22:31:06','Super Administrator',1,0),('208f156d4a803025c284bb595a7576b4','enable','Mengaktifkan data.','2019-07-01 20:43:47','System',NULL,NULL,1,1),('3ac340832f29c11538fbe2d6f75e8bcc','update','Mengubah data.','2019-07-03 01:05:39','System','2019-07-10 20:46:13',NULL,1,0),('76ea0bebb3c22822b4f0dd9c9fd021c5','create','Membuat data baru.','2019-07-03 01:05:26','System','2019-07-10 20:57:38','Super Administrator',1,0),('921ea49aff675592b681df7d1a1391ae','export_excel','Ekspor data berupa file Excel.','2019-08-18 22:30:42','Super Administrator','2019-08-18 22:30:57','Super Administrator',1,0),('ecae13117d6f0584c25a9da6c8f8415e','read','Membuat data baru.','2019-07-03 01:05:00','System','2019-07-10 20:57:38','Super Administrator',1,0);
/*!40000 ALTER TABLE `ap_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ap_user`
--

DROP TABLE IF EXISTS `ap_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ap_user` (
  `id` varchar(32) NOT NULL,
  `group_id` varchar(32) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ap_user`
--

LOCK TABLES `ap_user` WRITE;
/*!40000 ALTER TABLE `ap_user` DISABLE KEYS */;
INSERT INTO `ap_user` VALUES ('17c4520f6cfd1ab53d8745e84681eb49','c4ca4238a0b923820dcc509a6f75849b','superadmin','7363a0d0604902af7b70b271a0b96480','Super Administrator','2019-07-02 01:36:21','System','2019-08-08 20:20:03','Super Administrator',1,0),('1fbf75e9eceea88adcfc6ad77dbf04cc','849176481c855a4e1462d772536d3c16','listrik','7363a0d0604902af7b70b271a0b96480','User Kelistrikan','2019-11-05 02:09:32','Super Administrator',NULL,NULL,1,0),('4859774d852c6013660d159d27c7e715','c4979eca0b1dc4067e2ef4a7bd05f84c','atk','7363a0d0604902af7b70b271a0b96480','User ATK','2019-11-05 02:06:40','Super Administrator',NULL,NULL,1,0),('4ceb97ddcf8bce84f7608b90e8533d70','2b18ed0963796806452ad12fc4e9ee0a','cetak','7363a0d0604902af7b70b271a0b96480','User Percetakan','2019-11-05 02:08:27','Super Administrator',NULL,NULL,1,0),('7726300802910b937f24028a380f4de0','79abf252b0c4c5747c39fd7a69e13832','kebersihan','7363a0d0604902af7b70b271a0b96480','User Kebersihan','2019-11-05 02:05:37','Super Administrator','2019-11-05 02:06:56','Super Administrator',1,0),('b785a32d7e8e37635823a27fbb13cdf9','49fe31429754ff30f1c853b4f87918d2','farmasi','7363a0d0604902af7b70b271a0b96480','User Farmasi','2019-11-05 01:59:44','Super Administrator','2019-11-05 02:07:15','Super Administrator',1,0);
/*!40000 ALTER TABLE `ap_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_ekonomi`
--

DROP TABLE IF EXISTS `dt_ekonomi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_ekonomi` (
  `id` varchar(32) NOT NULL,
  `tahun` int(4) NOT NULL DEFAULT '0',
  `nilai` float NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_ekonomi`
--

LOCK TABLES `dt_ekonomi` WRITE;
/*!40000 ALTER TABLE `dt_ekonomi` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_ekonomi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_inflasi`
--

DROP TABLE IF EXISTS `dt_inflasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_inflasi` (
  `id` varchar(32) NOT NULL,
  `tahun` int(4) NOT NULL DEFAULT '0',
  `nilai` float NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_inflasi`
--

LOCK TABLES `dt_inflasi` WRITE;
/*!40000 ALTER TABLE `dt_inflasi` DISABLE KEYS */;
INSERT INTO `dt_inflasi` VALUES ('9000d7a971e527b2616a361d34b45d17',2011,28,'2019-11-12 02:06:35','Super Administrator',NULL,NULL,1,0),('9a5d72cbdf3e31a912c7a3ce90d27299',2010,25,'2019-11-12 02:06:27','Super Administrator',NULL,NULL,1,0),('d551216f3b22b6a62a8b95a93174ed0d',2012,21,'2019-11-12 02:06:43','Super Administrator',NULL,NULL,1,0),('e0b5e794c716ecb334d84b637c9024cd',2013,19,'2019-11-12 02:06:56','Super Administrator',NULL,NULL,1,0);
/*!40000 ALTER TABLE `dt_inflasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_ipm`
--

DROP TABLE IF EXISTS `dt_ipm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_ipm` (
  `id` varchar(32) NOT NULL,
  `tahun` int(4) NOT NULL DEFAULT '0',
  `nilai` float NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_ipm`
--

LOCK TABLES `dt_ipm` WRITE;
/*!40000 ALTER TABLE `dt_ipm` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_ipm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_kategori`
--

DROP TABLE IF EXISTS `dt_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_kategori` (
  `id` varchar(32) NOT NULL,
  `tahun_id` varchar(32) NOT NULL,
  `kategori` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_kategori`
--

LOCK TABLES `dt_kategori` WRITE;
/*!40000 ALTER TABLE `dt_kategori` DISABLE KEYS */;
INSERT INTO `dt_kategori` VALUES ('091c65f84077591b5110f5ae75fc2f46','740275419f61a5fa709935138452a55a','PERTANIAN','','2019-11-06 01:34:29','Super Administrator',NULL,NULL,1,0);
/*!40000 ALTER TABLE `dt_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_kemiskinan`
--

DROP TABLE IF EXISTS `dt_kemiskinan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_kemiskinan` (
  `id` varchar(32) NOT NULL,
  `tahun` int(4) NOT NULL DEFAULT '0',
  `nilai` float NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_kemiskinan`
--

LOCK TABLES `dt_kemiskinan` WRITE;
/*!40000 ALTER TABLE `dt_kemiskinan` DISABLE KEYS */;
INSERT INTO `dt_kemiskinan` VALUES ('',2010,21,'2019-11-12 02:25:02','Super Administrator',NULL,NULL,1,0);
/*!40000 ALTER TABLE `dt_kemiskinan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_penduduk`
--

DROP TABLE IF EXISTS `dt_penduduk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_penduduk` (
  `id` varchar(32) NOT NULL,
  `tahun` int(4) NOT NULL DEFAULT '0',
  `nilai` float NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_penduduk`
--

LOCK TABLES `dt_penduduk` WRITE;
/*!40000 ALTER TABLE `dt_penduduk` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_penduduk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_pengangguran`
--

DROP TABLE IF EXISTS `dt_pengangguran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_pengangguran` (
  `id` varchar(32) NOT NULL,
  `tahun` int(4) NOT NULL DEFAULT '0',
  `nilai` float NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_pengangguran`
--

LOCK TABLES `dt_pengangguran` WRITE;
/*!40000 ALTER TABLE `dt_pengangguran` DISABLE KEYS */;
/*!40000 ALTER TABLE `dt_pengangguran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_subkategori`
--

DROP TABLE IF EXISTS `dt_subkategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_subkategori` (
  `id` varchar(32) NOT NULL,
  `tahun_id` varchar(32) NOT NULL,
  `kategori_id` varchar(32) NOT NULL,
  `jenis` tinyint(1) DEFAULT '0',
  `subkategori` varchar(50) NOT NULL DEFAULT '0',
  `berkas` varchar(50) NOT NULL DEFAULT '0',
  `url` text,
  `description` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_subkategori`
--

LOCK TABLES `dt_subkategori` WRITE;
/*!40000 ALTER TABLE `dt_subkategori` DISABLE KEYS */;
INSERT INTO `dt_subkategori` VALUES ('5581ae9ef8e46208eed4763034f8ee4c','740275419f61a5fa709935138452a55a','091c65f84077591b5110f5ae75fc2f46',1,'Jumlah Petani berdasarkan Umur','0','https://www.google.com/','','2020-10-14 06:11:55','Super Administrator',NULL,NULL,1,0),('fbabe48710afa3489ec79f79d7ffc564','740275419f61a5fa709935138452a55a','091c65f84077591b5110f5ae75fc2f46',0,'Jumlah petani per kecamatan','e9e8616b0a95b5e2b27a2ed27fcabf94.pdf','','','2019-11-11 05:59:51','Super Administrator','2020-10-14 05:36:25','Super Administrator',1,0);
/*!40000 ALTER TABLE `dt_subkategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dt_tahun`
--

DROP TABLE IF EXISTS `dt_tahun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dt_tahun` (
  `id` varchar(32) NOT NULL,
  `tahun` int(4) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_tahun`
--

LOCK TABLES `dt_tahun` WRITE;
/*!40000 ALTER TABLE `dt_tahun` DISABLE KEYS */;
INSERT INTO `dt_tahun` VALUES ('71927ud8a9sd8w789qwdu89w7e89we',2020,'KKDA 2020','2019-11-11 04:49:35','System',NULL,NULL,1,0),('740275419f61a5fa709935138452a55a',2019,'KKDA 2019','2019-11-06 01:33:56','Super Administrator',NULL,NULL,1,0);
/*!40000 ALTER TABLE `dt_tahun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'kada'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-14 13:47:44
