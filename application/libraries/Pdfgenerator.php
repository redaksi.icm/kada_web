<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('DOMPDF_ENABLE_AUTOLOAD', false);
require_once("./vendor/dompdf/dompdf/dompdf_config.inc.php");

class Pdfgenerator extends DOMPDF{

  public function generate($html, $filename='', $stream=TRUE, $orientation = "landscape")
  {
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->set_paper(array(0,0,609.4488,935.433), $orientation);
    $dompdf->render();
    $canvas = $dompdf ->get_canvas();
    $canvas->page_text(20, 590, "simasfarm_".date('YmdHis'), null, 7, array(0, 0, 0));
    $canvas->page_text(850, 590, "Halaman {PAGE_NUM} dari {PAGE_COUNT}", null, 7, array(0, 0, 0));
    if ($stream) {
      $dompdf->stream($filename.".pdf", array("Attachment" => 0));
    } else {
      return $dompdf->output();
    }
  }
}