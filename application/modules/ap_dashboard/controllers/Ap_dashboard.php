<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_dashboard extends MY_Controller {

	function __construct(){
		parent::__construct();

		$controller = 'ap_dashboard';

    if($this->session->userdata('controller') != $controller){
      $this->session->unset_userdata('search');
      $this->session->unset_userdata('order');
      $this->session->unset_userdata('per_page');
      $this->session->set_userdata(array('controller' => $controller));
		}
    $this->load->model('ap_config/m_ap_config');
		$this->group_id = $this->session->userdata('group_id');
		$this->menu = $this->m_ap_config->get_menu($this->group_id,$controller);
		$this->access = $this->m_ap_config->get_access($this->group_id,$controller);
		
		if ($this->menu == null) {
			redirect(base_url().'ap_error/error_403');
		}

		// $this->load->model('m_ap_dashboard');
	}

	public function index()
	{
		if (in_array('read', $this->access)) {
			$data['title'] = "Ringkasan";
			$data['menu'] = $this->menu;
			$this->render('index', $data);
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}