<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <!-- <div class="pageheader">
    <h3><i class="fa fa-home"></i> <?=$title?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active"> <?=$menu->menu?> </li>
      </ol>
    </div>
  </div> -->
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="alert alert-success media fade in">
      <div class="media-left">
        <span class="icon-wrap icon-wrap-xs icon-circle alert-icon">
          <i class="fa fa-user fa-lg"></i>
        </span>
      </div>
      <div class="media-body">
        <h4 class="alert-title">Selamat Datang.</h4>
        <p class="alert-message">
          Anda login sebagai <b><?=$this->session->userdata('fullname');?></b>. 
          Untuk keluar dari aplikasi silakan klik 
          <a href="<?=base_url()?>ap_auth/logout/action">Logout.</a>
        </p>
      </div>
    </div>
  </div>
  <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
      