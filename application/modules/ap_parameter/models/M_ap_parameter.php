<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_parameter extends CI_Model {

	public function get_list($number,$offset,$search = null, $order = null)
  {
    $where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.parameter LIKE '%".$search['term']."%' ";
        $where .= "OR a.value LIKE '%".$search['term']."%' ";
      }
    }

    if ($order == null) {
      $order['order_field'] = 'created';
      $order['order_type'] = 'asc';
      $this->session->set_userdata(array('order' => $order));
    }
    
    return $this->db->query(
      "SELECT * FROM ap_parameter a 
      $where
      ORDER BY "
        .$order['order_field']." ".$order['order_type'].
      " LIMIT 
        $offset, $number"
    );
  }

  function num_rows($search = null){
		$where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.parameter LIKE '%".$search['term']."%' ";
        $where .= "OR a.value LIKE '%".$search['term']."%' ";
      }
    }
    
    return $this->db->query(
      "SELECT * FROM ap_parameter a 
      $where"
    )->num_rows();
  }
  
  function num_rows_total($search = null){
    return $this->db->query(
      "SELECT * FROM ap_parameter a 
      WHERE is_deleted = 0"
    )->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted',0)
      ->where('is_active',1)
      ->order_by('created','asc')
			->get('ap_parameter')->result();
  }
  
  public function get_by_parameter($parameter)
  {
    return $this->db->where('parameter',$parameter)->get('ap_parameter')->row();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('ap_parameter')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('ap_parameter')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('ap_parameter')->row();
  }

  public function create($data)
  {
    $this->db->insert('ap_parameter',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('ap_parameter',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id',$id)->update('ap_parameter',array('is_deleted' => 1));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('ap_parameter');
  }

  public function enable($id)
  {
    $this->db->where('id',$id)->update('ap_parameter',array('is_active' => 1));
  }

  public function disable($id)
  {
    $this->db->where('id',$id)->update('ap_parameter',array('is_active' => 0));
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_parameter");
  }

}
