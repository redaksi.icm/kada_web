<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i> <?=@$title?> <?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Pengaturan
        </li>
        <li class="active">
          Master Menu
        </li>
        <li class="active">
          <?=$menu->menu?>
        </li>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Daftar <?=@$menu->menu?></h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-3 col-xs-12">
            <?php if(in_array('create', $access)):?>
              <a class="btn btn-sm btn-primary pull-left" href="<?=base_url().$menu->controller?>/form"><i class="fa fa-plus"></i> Tambah</a>
            <?php endif; ?>
          </div>
          <form action="<?=base_url().$menu->controller?>/search" method="post" autocomplete="off">
            <div class="col-md-3 col-xs-12 col-md-offset-6">
              <div class="input-group">
                <input type="text" name="term" class="form-control input-sm" placeholder="Parameter / Nilai" value="<?php if($search != null){echo $search['term'];}?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-search"></i></button>
                  <a class="btn btn-sm btn-default btn-flat" href="<?=base_url().$menu->controller?>/reset"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="row row-info">
          <div class="col-md-6">
            Tampilkan 
            <div class="btn-group btn-group-xs">
              <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                <?=@$per_page?> <i class="dropdown-caret fa fa-caret-down"></i>
              </button>
              <ul class="dropdown-menu">
                <li class="<?php if($per_page == 10){echo 'active';}?>"><a href="<?=base_url($menu->controller)?>/per_page/10">10</a></li>
                <li class="<?php if($per_page == 25){echo 'active';}?>"><a href="<?=base_url($menu->controller)?>/per_page/25">25</a></li>
                <li class="<?php if($per_page == 50){echo 'active';}?>"><a href="<?=base_url($menu->controller)?>/per_page/50">50</a></li>
                <li class="<?php if($per_page == 100){echo 'active';}?>"><a href="<?=base_url($menu->controller)?>/per_page/100">100</a></li>
              </ul>
            </div>
            data.
          </div>
          <div class="col-md-6">
            <div class="pagination-info text-right">
              <?=@$pagination_info?>
            </div>
          </div>
        </div>
        <div class="line-separator"></div>
        <div class="flash-data" data-flashdata="<?=$this->session->flashdata('flash')?>"></div>
        <div class="row">
          <div class="col-md-12 table-responsive">
            <table class="table table-striped table-bordered table-condensed">
              <thead>
                <?php 
                  if ($order['order_type'] == 'asc') {
                    $ic_order = '-asc';
                    $url_order = 'desc';
                  }else{
                    $ic_order = '-desc';
                    $url_order = 'asc';
                  }
                ?>
                <tr>
                  <th class="text-center" width="40">No.</th>
                  <?php if(in_array('update',$access) || in_array('delete',$access)):?>
                    <th class="text-center" width="10">
                      <label class="checkall-label form-checkbox form-icon form-primary form-text">
                        <input class="checkall" type="checkbox" onchange="checkAll(this)">
                      </label>
                    </th>
                  <?php endif; ?>
                  <?php if(in_array('update',$access) || in_array('delete',$access)):?>
                    <th class="text-center" width="50">Aksi</th>
                  <?php endif; ?>
                  <th class="text-center">
                    <?php if($order['order_field'] == 'parameter'):?>
                      <a href="<?=base_url().$menu->controller.'/order/parameter/'.$url_order?>">
                        Parameter <i class="fa fa-sort<?=$ic_order?>"></i>
                      </a>
                    <?php else: ?>
                      <a href="<?=base_url().$menu->controller.'/order/parameter/asc'?>">
                        Parameter <i class="fa fa-sort"></i>
                      </a>
                    <?php endif; ?>
                  </th>
                  <th class="text-center">
                    <?php if($order['order_field'] == 'value'):?>
                      <a href="<?=base_url().$menu->controller.'/order/value/'.$url_order?>">
                        Nilai <i class="fa fa-sort<?=$ic_order?>"></i>
                      </a>
                    <?php else: ?>
                      <a href="<?=base_url().$menu->controller.'/order/value/asc'?>">
                        Nilai <i class="fa fa-sort"></i>
                      </a>
                    <?php endif; ?>
                  </th>
                  <th class="text-center" width="70">
                    <?php if($order['order_field'] == 'is_active'):?>
                      <a href="<?=base_url().$menu->controller.'/order/is_active/'.$url_order?>">
                        Status <i class="fa fa-sort<?=$ic_order?>"></i>
                      </a>
                    <?php else: ?>
                      <a href="<?=base_url().$menu->controller.'/order/is_active/asc'?>">
                        Status <i class="fa fa-sort"></i>
                      </a>
                    <?php endif; ?>
                  </th>
                </tr>
              </thead>
              <?php if ($main != null): ?>
                <form id="form-multiple" action="" method="post">
                  <tbody>
                    <?php $i=1;foreach ($main as $row): ?>
                      <tr>
                        <td><?=$from+$i++?></td>
                        <?php if(in_array('update',$access) || in_array('delete',$access)):?>
                          <td class="text-center" width="10">
                            <label class="checkitem-label form-checkbox form-icon form-primary form-text">
                              <input class="checkitem" type="checkbox" name="checkitem[]" value="<?=$row->id?>" onchange="checkItemAll()">
                            </label>
                          </td>
                        <?php endif; ?>
                        <?php if(in_array('update',$access) || in_array('delete',$access)):?>
                          <td class="text-center">
                            <?php if(in_array('update', $access)):?>
                              <a class="btn btn-xs btn-warning add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Ubah" href="<?=base_url().$menu->controller?>/form/<?=$row->id?>"><i class="fa fa-pencil"></i></a>
                            <?php endif; ?>
                          </td>
                        <?php endif; ?>
                        <td><b><?=$row->parameter?></b></td>
                        <td><?=$row->value?></td>
                        <td class="text-center td-status">
                          <?php if(in_array('update', $access)):?>
                            <?php if ($row->is_active == 1): ?>
                              <a href="<?=base_url($menu->controller)?>/disable/<?=@$row->id?>">
                                <i class="fa fa-toggle-on status-icon text-success"></i>
                              </a>
                              <?php else: ?>
                              <a href="<?=base_url($menu->controller)?>/enable/<?=@$row->id?>">
                                <i class="fa fa-toggle-off status-icon text-default"></i>
                              </a>
                            <?php endif; ?>
                          <?php else: ?>
                            <?php if ($row->is_active == 1): ?>
                              <i class="fa fa-toggle-on status-icon text-success"></i>
                            <?php else: ?>
                              <i class="fa fa-toggle-off status-icon text-default"></i>
                            <?php endif; ?>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </form>
                </tbody>
                <?php else: ?>
                <tbody>
                  <tr>
                    <td class="text-center" colspan="99">Tidak ada data!</td>
                  </tr>
                </tbody>
              <?php endif; ?>
            </table>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <?php if(in_array('update',$access) || in_array('delete',$access)):?>  
              <div class="btn-group btn-group-xs">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="false">
                  Aksi Multiple <i class="dropdown-caret fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu">
                  <?php if(in_array('update',$access)):?>
                    <li><a href="javascript:multipleAction('enable')">Aktif</a></li>
                    <li><a href="javascript:multipleAction('disable')">Non Aktif</a></li>
                  <?php endif; ?>
                  <?php if(in_array('delete',$access)):?>
                    <!-- <li><a href="javascript:multipleAction('delete')">Hapus</a></li> -->
                  <?php endif; ?>
                </ul>
              </div>
            <?php endif; ?>
          </div>
          <div class="col-md-6 col-sm-12">
            <?php echo $this->pagination->create_links(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->