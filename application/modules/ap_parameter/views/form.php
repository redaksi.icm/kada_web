<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i> <?=@$title?> <?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Pengaturan
        </li>
        <li class="active">
          Master Menu
        </li>
        <li class="active">
          <a href="<?=base_url($menu->controller)?>">
            <?=$menu->menu?>
          </a>
        </li>
        <li>
          <?php if($action == 'create'):?>
            Tambah
          <?php else: ?>
            Ubah
          <?php endif; ?>
        </li>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Form <?=@$menu->menu?></h3>
      </div>
      <form id="form" class="form-horizontal" action="<?=base_url().$menu->controller?>/<?=$action?>" method="post" autocomplete="off">
        <input type="hidden" name="id" value="<?=@$main->id?>">
        <div class="panel-body">
          <fieldset>
            <div class="form-group">
              <label class="col-lg-2 control-label">Parameter <span class="text-danger">*</span></label>
              <div class="col-lg-4">
                <input type="text" class="form-control input-sm" name="parameter" id="parameter" value="<?=@$main->parameter?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Nilai <span class="text-danger">*</span></label>
              <div class="col-lg-5">
                <input type="text" class="form-control input-sm" name="value" id="value" value="<?=@$main->value?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Aktif</label>
              <div class="col-lg-5">
                <label class="form-checkbox form-icon <?php if($main){if($main->is_active == 1){echo 'active';}}else{echo 'active';}?> form-text">
                  <input type="checkbox" name="is_active" value="1" <?php if($main){if($main->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
                </label>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-sm-7 col-sm-offset-2">
              <button class="btn btn-sm btn-primary btn-action" type="submit"><i class="fa fa-save"></i> Simpan</button>
              <a class="btn btn-sm btn-flat btn-default" href="<?=base_url($menu->controller)?>/index"><i class="fa fa-close"></i> Batal</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- End Page content -->
</div>
<!--END CONTENT CONTAINER-->
<script>
  var faIcon = {
    valid: 'fa fa-check-circle fa-lg text-success',
    invalid: 'fa fa-times-circle fa-lg',
    validating: 'fa fa-refresh'
  }
  $('#form').bootstrapValidator({
    live: 'enabled',
		message: 'Nilai tidak valid.',
		feedbackIcons: faIcon,
		fields: {
      
    }
	}).on('success.field.bv', function(e, data) {
		// $(e.target)  --> The field element
		// data.bv      --> The BootstrapValidator instance
		// data.field   --> The field name
		// data.element --> The field element

		var $parent = data.element.parents('.form-group');

		// Remove the has-success class
		$parent.removeClass('has-success');
	}).on('success.form.bv', function(e) {
    // Make button action proses
    $('.btn-action').html('<i class="fa fa-spin fa-spinner"></i> Proses...');
  });
</script>