<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_menu extends CI_Model {

	public function get_list($number,$offset,$search = null, $order = null)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.type LIKE '%".$search['term']."%' ";
        $where .= "OR a.menu LIKE '%".$search['term']."%' ";
      }
    }

    if ($order == null) {
      $order['order_field'] = 'id';
      $order['order_type'] = 'asc';
      $this->session->set_userdata(array('order' => $order));
    }
    
    return $this->db->query(
      "SELECT a.*, count(b.menu_id) as count from ap_menu a 
      LEFT JOIN ap_menu_role b on a.id = b.menu_id
      $where
      GROUP BY a.id
      ORDER BY "
        .$order['order_field']." ".$order['order_type'].
      " LIMIT 
        $offset, $number"
    );
  }

  function num_rows($search = null){
		$where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.type LIKE '%".$search['term']."%' ";
        $where .= "OR a.menu LIKE '%".$search['term']."%' ";
      }
    }
    
    return $this->db->query(
      "SELECT * FROM ap_menu a 
      $where"
    )->num_rows();
  }
  
  function num_rows_total($search = null){
    return $this->db->query(
      "SELECT * FROM ap_menu a 
      WHERE is_deleted = 0"
    )->num_rows();
  }

	public function get_all()
	{
		return $this->db
			->where('is_deleted',0)
			->where('is_active',1)
			->get('ap_menu')->result();
  }

  public function get_role($id)
  {
    return $this->db->query(
      "SELECT a.id, a.role, b.menu_id FROM ap_role a
      LEFT JOIN ap_menu_role b ON a.id = b.role_id AND b.menu_id = '$id'
      WHERE a.is_deleted = 0
      ORDER BY a.created ASC"
    )->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('ap_menu')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('ap_menu')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('ap_menu')->row();
  }

  public function create($data)
  {
    if ($data['type'] == 2) {
      $d_menu = $data;
      unset($d_menu['role_id']);
      $this->db->insert('ap_menu',$d_menu);
        
      foreach ($data['role_id'] as $key) {
        $d_role = array(
          'menu_id' => $data['id'],
          'role_id' => $key,
          'created' => $data['created'],
          'created_by' => $data['created_by']
        );
        $this->db->insert('ap_menu_role', $d_role);
      }
    }else{
      unset($data['role_id']);
      $this->db->insert('ap_menu',$data);
    }
  }

  public function update($id,$data)
  {
    if ($data['type'] == 2) {
      $d_menu = $data;
      unset($d_menu['role_id']);
      $this->db->where('id',$id)->update('ap_menu',$d_menu);
      $this->db->where('menu_id', $id)->delete('ap_menu_role');
      //update group menu
      $this->db->where('menu_id', $id)->update('ap_group_menu', array('menu_id' => $data['id']));
      $this->db->where('menu_id', $id)->update('ap_group_role', array('menu_id' => $data['id']));
  
      foreach ($data['role_id'] as $key) {
        $d_role = array(
          'menu_id' => $data['id'],
          'role_id' => $key,
          'created' => $data['updated'],
          'created_by' => $data['updated_by']
        );
        $this->db->insert('ap_menu_role', $d_role);
      }
    }else{
      unset($data['role_id']);
      $this->db->where('id',$id)->update('ap_menu',$data);
      $this->db->where('menu_id', $id)->delete('ap_menu_role');
    }
  }

  public function delete_temp($id)
  {
    $this->db->where('id',$id)->update('ap_menu',array('is_deleted' => 1));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('ap_menu');
    $this->db->where('menu_id',$id)->delete('ap_menu_role');
    $this->db->where('menu_id', $id)->delete('ap_group_menu');
    $this->db->where('menu_id', $id)->delete('ap_group_role');
  }

  public function enable($id)
  {
    $this->db->where('id',$id)->update('ap_menu',array('is_active' => 1));
  }

  public function disable($id)
  {
    $this->db->where('id',$id)->update('ap_menu',array('is_active' => 0));
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_menu");
  }

}
