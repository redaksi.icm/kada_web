<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=@$profile->singkatan?> - <?=@$profile->aplikasi?></title>
    <link rel="shortcut icon" href="<?=base_url()?>/img/<?=@$profile->logo?>" type="image/x-icon">
    <!--STYLESHEET-->
    <!--Roboto Slab Font [ OPTIONAL ] -->
    <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:500,400italic,100,700italic,300,700,500italic,400" rel="stylesheet">
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="<?=base_url()?>css/style.css" rel="stylesheet">
    <!--Font Awesome [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Sweetalert2 -->
    <link href="<?=base_url()?>plugins/sweetalert2/dist/sweetalert2.css" rel="stylesheet"/>
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">
    <style>
      .cls-container{
        background: linear-gradient(0deg,rgba(52, 152, 219, 0.8),rgba(52, 152, 219, 0.8)), url('<?=base_url()?>/img/bg.jpg') !important;
      }
    </style>
    <!--SCRIPT-->
    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/pace/pace.min.css" rel="stylesheet">
    <script src="<?=base_url()?>plugins/pace/pace.min.js"></script>
    <!--jQuery [ REQUIRED ]-->
    <script src="<?=base_url()?>js/jquery-2.1.1.min.js"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?=base_url()?>js/bootstrap.min.js"></script>
    <!-- Sweetalert2 -->
    <script src="<?=base_url()?>plugins/sweetalert2/dist/sweetalert2.js"></script>
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <script src="<?=base_url()?>plugins/bootstrap-validator/bootstrapValidator.min.js"></script>
    <script src="<?=base_url()?>plugins/bootstrap-validator/id_ID.js"></script>
  </head>
  <body>
    <div id="container" class="cls-container">
      <div class="lock-wrapper" style="margin-top:20vh">
        <div class="panel lock-box">
          <div class="center"> <img alt="<?=base_url()?>/img/<?=@$profile->logo?>" src="<?=base_url()?>/img/<?=@$profile->logo?>" height="80" /> </div>
          <h4> <?=@$profile->aplikasi?> </h4>
          <p class="text-center">Silakan masuk untuk mengakses aplikasi.</p>
          <div class="error-flash-data" data-flashdata="<?=$this->session->flashdata('flash')?>"></div>
          <form id="form" class="-inline" role="form" action="<?=base_url()?>ap_auth/login/action" method="post" autocomplete="off">
            <div class="form-group">
              <div class="text-left">
                <input class="form-control input-sm" type="text" id="user_name" name="user_name" placeholder="Nama Pengguna"  required="required">
              </div>
            </div>
            <div class="form-group">
              <div class="text-left">
                <input class="form-control input-sm lock-input" type="password" id="user_password" name="user_password" placeholder="Kata Sandi"  required="required">
              </div>
            </div>
            <button type="submit" class="btn btn-sm btn-primary btn-action btn-block"><i class="fa fa-sign-in"></i> Masuk</button>
          </form>
        </div>
      </div>
    </div>
    <!-- END OF CONTAINER -->
    <!--Jasmine Admin [ RECOMMENDED ]-->
    <script src="<?=base_url()?>js/scripts.js"></script>
    <script>
      // error message
      const errorFlashData = $('.error-flash-data').data('flashdata');
      if (errorFlashData) {
        Swal.fire({
          title : 'Gagal Masuk',
          text : errorFlashData,
          type : 'error',
          timer : 1500,
          showConfirmButton : false
        });
      }
      // validators
      var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
      }
      $('#form').bootstrapValidator({
        live: 'enabled',
        message: 'Nilai tidak valid.',
        fields: {
          username: {
            validators: {
              notEmpty: {
                message: 'Nama pengguna tidak boleh kosong.'
              }
            }
          },
          password: {
            validators: {
              notEmpty: {
                message: 'Kata sandi tidak boleh kosong.'
              }
            }
          }
        }
      }).on('success.field.bv', function(e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
      }).on('success.form.bv', function(e) {
        // Make button action proses
        $('.btn-action').html('<i class="fa fa-spin fa-spinner"></i> Proses...');
      });
    </script>
  </body>
</html>