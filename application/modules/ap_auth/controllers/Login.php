<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	function __construct(){
    parent::__construct();
		$this->load->model('ap_profile/m_ap_profile');
		$this->load->model('ap_activity/m_ap_activity');
  }

	public function index()
	{
		$this->load->library('user_agent');
		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_robot()){
			$agent = $this->agent->robot();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Unidentified';
		}

		$data_log = array(
			"user_id" => '-',
			"sess_id" => $this->session->session_id,
			"fullname" => '-',
			"role" => 'access',
			"ip_address" => $this->input->ip_address(),
			"user_agent" => $agent,
			"platform" => $this->agent->platform(),
			"module" => 'Login',
			"description" => 'Mengakses aplikasi.',
			"created" => date('Y-m-d H:i:s')
		);
		
		$this->m_ap_activity->create($data_log);
		$data['profile'] = $this->m_ap_profile->get_first();
	  $this->load->view('login', $data);
	}
	
	public function action()
	{
		$data = $this->input->post(null,true);
		if ($data != null) {
			$data['user_password'] = md5(md5(md5($data['user_password'])));
			$tahun = $data['tahun'];
			var_dump($data);
			unset($data['tahun']);
			
			$this->load->model('ap_auth/m_ap_login');
			$this->load->model('ap_activity/m_ap_activity');
			$this->load->model('ap_config/m_ap_config');

			$result = $this->m_ap_login->action($data);

			if ($this->agent->is_browser()){
				$agent = $this->agent->browser().' '.$this->agent->version();
			}elseif ($this->agent->is_robot()){
				$agent = $this->agent->robot();
			}elseif ($this->agent->is_mobile()){
				$agent = $this->agent->mobile();
			}else{
				$agent = 'Unidentified';
			}
			
			if($result->num_rows() >= 1){
				foreach ($result->result() as $sess) {
					$sess_data['user_id'] = $sess->id;
					$sess_data['group_id'] = $sess->group_id;
					$sess_data['fullname'] = $sess->fullname;
					$sess_data['tahun'] = $tahun;
					
					$data_log = array(
						"user_id" => $sess->id,
						"sess_id" => $this->session->session_id,
						"fullname" => $sess->fullname,
						"role" => 'login',
						"ip_address" => $this->input->ip_address(),
						"user_agent" => $agent,
						"platform" => $this->agent->platform(),
						"module" => 'Login',
						"description" => 'Masuk ke sistem',
						"created" => date('Y-m-d H:i:s')
					);
					$this->m_ap_activity->create($data_log);
				}

				$sess_data['logged'] = true;
				$this->session->set_userdata($sess_data);

				$menu = $this->m_ap_config->get_first_menu($sess->group_id);
				redirect(base_url().$menu->controller);
			}else{
				$data_log = array(
					"user_id" => '-',
					"sess_id" => $this->session->session_id,
					"fullname" => $data['user_name'],
					"role" => 'login',
					"ip_address" => $this->input->ip_address(),
					"user_agent" => $agent,
					"platform" => $this->agent->platform(),
					"module" => 'Login',
					"description" => 'Gagal masuk ke sistem',
					"created" => date('Y-m-d H:i:s')
				);
				$this->m_ap_activity->create($data_log);
				$this->session->set_flashdata('flash', 'Nama Pengguna dan Kata Sandi tidak cocok.');
				redirect(base_url().'ap_auth/login');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}
  
}
