<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {

	public function action()
	{
		$this->load->model('ap_activity/m_ap_activity');
		
		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_robot()){
			$agent = $this->agent->robot();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Unidentified';
		}

		$data_log = array(
			"user_id" => $this->session->userdata('user_id'),
			"sess_id" => $this->session->session_id,
			"fullname" => $this->session->userdata('fullname'),
			"role" => 'logout',
			"ip_address" => $this->input->ip_address(),
			"user_agent" => $agent,
			"platform" => $this->agent->platform(),
			"module" => 'Logout',
			"description" => 'Keluar dari sistem',
			"created" => date('Y-m-d H:i:s')
		);

		$this->m_ap_activity->create($data_log);
		$this->session->sess_destroy();
		redirect(base_url().'ap_auth/login');
	}
  
}
