<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i> <?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Menu Utama
        </li>
        <li class="active">
          <?=$menu->menu?>
        </li>
        <li class="active">
          Tahun
        </li>
        <li class="active">
          Kategori
        </li>
        <li class="active">
          Sub Kategori
        </li>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="tab-base">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="<?=base_url($menu->controller)?>/tahun"> <i class="fa fa-calendar"></i>  Tahun </a>
        </li>
        <li class="active">
          <a href="<?=base_url($menu->controller)?>/kategori/<?=$tahun->id?>"><i class="fa fa-list"></i> Kategori</a>
        </li>
        <li class="active">
          <a href="<?=base_url($menu->controller)?>/subkategori/<?=$tahun->id?>/<?=$kategori->id?>"><i class="fa fa-list-alt"></i> Sub Kategori</a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="" class="tab-pane fade active in">
          <h4 class="text-thin">Daftar Sub Kategori (Tahun <?=$tahun->tahun?>) (Kategori : <?=$kategori->kategori?>)</h4>
          <div class="flash-data" data-flashdata="<?=$this->session->flashdata('flash')?>"></div>
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <?php if(in_array('create', $access)):?>
              <a class="btn btn-sm btn-success pull-left" href="<?=base_url($menu->controller)?>/kategori/<?=$tahun->id?>" style="margin-right:5px;"><i class="fa fa-arrow-left"></i></a> 
              <a class="btn btn-sm btn-primary pull-left" href="<?=base_url().$menu->controller?>/subkategori_form/<?=$tahun->id?>/<?=$kategori->id?>"><i class="fa fa-plus"></i> Tambah Sub Kategori</a>
            <?php endif; ?>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12 table-responsive">
            <table id="table" class="table table-striped table-bordered table-condensed">
              <thead>
                <tr>
                  <th class="text-center" width="40">No.</th>
                  <th class="text-center" width="50">Aksi</th>
                  <th class="text-center" width="50">Tahun</th>
                  <th class="text-center" width="150">Kategori</th>
                  <th class="text-center">Sub Kategori</th>
                  <th class="text-center" width="50">Jenis</th>
                  <th class="text-center" width="10">Berkas</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1;foreach ($main as $row): ?>
                  <tr>
                    <td class="text-center"><?=$i++?></td>
                    <?php if(in_array('update',$access) || in_array('delete',$access)):?>
                      <td class="text-center">
                        <?php if(in_array('update', $access)):?>
                          <a class="btn btn-xs btn-warning add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Ubah" href="<?=base_url().$menu->controller?>/subkategori_form/<?=$row->tahun_id?>/<?=$row->kategori_id?>/<?=$row->id?>"><i class="fa fa-pencil"></i></a>
                        <?php endif; ?>
                        <?php if(in_array('delete', $access)):?>
                          <a class="btn btn-xs btn-danger btn-delete add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Hapus" href="<?=base_url().$menu->controller?>/subkategori_delete/<?=$row->tahun_id?>/<?=$row->kategori_id?>/<?=$row->id?>"><i class="fa fa-trash"></i></a>
                        <?php endif; ?>
                      </td>
                    <?php endif; ?>
                    <td class="text-center"><b><?=$row->tahun?></b></td>
                    <td><?=$row->kategori?></td>
                    <td><?=$row->subkategori?></td>
                    <td class="text-center">
                      <?php if($row->jenis == 0):?>
                        Berkas
                      <?php else:?>
                        Website
                      <?php endif;?>
                    </td>
                    <td class="text-center">
                      <?php if($row->jenis == 0):?>   
                        <a class="btn btn-success btn-xs" target="_blank" href="<?=base_url('berkas')?>/<?=$row->berkas?>">
                          <i class="fa fa-download"></i>
                        </a>
                      <?php else:?>
                        <a class="btn btn-success btn-xs" target="_blank" href="<?=$row->url?>">
                          <i class="fa fa-globe"></i>
                        </a>
                      <?php endif;?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
<script>
  $(document).ready(function () {
    $("#table").dataTable();
  })
</script>