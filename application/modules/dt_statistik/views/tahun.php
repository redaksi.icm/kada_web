<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i><?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Menu Utama
        </li>
        <li class="active">
          <?=$menu->menu?>
        </li>
        <li class="active">
          Tahun
        </li>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="tab-base">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="<?=base_url($menu->controller)?>/tahun"> <i class="fa fa-calendar"></i>  Tahun </a>
        </li>
        <li class="disabled">
          <a href="#"><i class="fa fa-list"></i> Kategori</a>
        </li>
        <li class="disabled">
          <a href="#"><i class="fa fa-list-alt"></i> Sub Kategori</a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="" class="tab-pane fade active in">
          <h4 class="text-thin">Daftar Tahun</h4>
          <div class="flash-data" data-flashdata="<?=$this->session->flashdata('flash')?>"></div>
        <div class="row">
          <div class="col-md-3 col-xs-12">
            <?php if(in_array('create', $access)):?>
              <a class="btn btn-sm btn-primary pull-left" href="<?=base_url().$menu->controller?>/tahun_form"><i class="fa fa-plus"></i> Tambah Tahun</a>
            <?php endif; ?>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12 table-responsive">
            <table id="table" class="table table-striped table-bordered table-condensed">
              <thead>
                <tr>
                  <th class="text-center" width="40">No.</th>
                  <th class="text-center" width="120">Aksi</th>
                  <th class="text-center" width="80">Tahun</th>
                  <th class="text-center">Deskripsi</th>
                  <th class="text-center" width="100">Jml. Kategori</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1;foreach ($main as $row): ?>
                  <tr>
                    <td class="text-center"><?=$i++?></td>
                    <?php if(in_array('update',$access) || in_array('delete',$access)):?>
                      <td class="text-center">
                        <?php if(in_array('update', $access)):?>
                          <a class="btn btn-xs btn-success add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Kategori" href="<?=base_url().$menu->controller?>/kategori/<?=$row->id?>"><i class="fa fa-list"></i> Kategori</a>
                        <?php endif; ?>
                        <?php if(in_array('update', $access)):?>
                          <a class="btn btn-xs btn-warning add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Ubah" href="<?=base_url().$menu->controller?>/tahun_form/<?=$row->id?>"><i class="fa fa-pencil"></i></a>
                        <?php endif; ?>
                        <?php if(in_array('delete', $access)):?>
                          <a class="btn btn-xs btn-danger btn-delete add-tooltip" data-placement="top" data-toggle="tooltip" data-original-title="Hapus" href="<?=base_url().$menu->controller?>/tahun_delete/<?=$row->id?>"><i class="fa fa-trash"></i></a>
                        <?php endif; ?>
                      </td>
                    <?php endif; ?>
                    <td class="text-center"><b><?=$row->tahun?></b></td>
                    <td><?=$row->description?></td>
                    <td class="text-center"><?=$row->jumlah?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
<script>
  $(document).ready(function () {
    $("#table").dataTable();
  })
</script>