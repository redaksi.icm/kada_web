
<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i> <?=@$title?> <?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Menu Utama
        </li>
        <li class="active">
          <?=$menu->menu?>
        </li>
        <li class="active">
          Tahun
        </li>
        <li class="active">
          <?php if($action == 'tahun_create'):?>
            Tambah
          <?php else: ?>
            Ubah
          <?php endif; ?>
        </li>
      </ol>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="tab-base">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="<?=base_url($menu->controller)?>/tahun"> <i class="fa fa-calendar"></i>  Tahun </a>
        </li>
        <li class="active">
          <a href="<?=base_url($menu->controller)?>/kategori/<?=$tahun->id?>"><i class="fa fa-list"></i> Kategori</a>
        </li>
        <li class="disabled">
          <a href="#"><i class="fa fa-list-alt"></i> Sub Kategori</a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="" class="tab-pane fade active in">
          <h4 class="text-thin">Form <?php echo $aks = ($action == 'kategori_create') ? "Tambah" : "Ubah" ; ?> Kategori (Tahun <?=$tahun->tahun?>)</h4>
          <br>
          <div class="row">
            <div class="col-md-12">
              <form id="form" class="form-horizontal" action="<?=base_url().$menu->controller?>/<?=$action?>/<?=$tahun->id?>" method="post" autocomplete="off">
                <input type="hidden" name="id" value="<?=@$main->id?>">
                <div class="panel-body">
                  <fieldset>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Tahun <span class="text-danger">*</span></label>
                      <div class="col-lg-2">
                        <input type="number" class="form-control input-sm" value="<?=@$tahun->tahun?>" required readonly>
                        <input type="hidden" class="form-control input-sm" name="tahun_id" value="<?=@$tahun->id?>" required readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Kategori <span class="text-danger">*</span></label>
                      <div class="col-lg-5">
                        <input type="text" class="form-control input-sm" name="kategori" id="kategori" value="<?=@$main->kategori?>" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label">Aktif</label>
                      <div class="col-lg-5">
                        <label class="form-checkbox form-icon <?php if($main){if($main->is_active == 1){echo 'active';}}else{echo 'active';}?> form-text">
                          <input type="checkbox" name="is_active" value="1" <?php if($main){if($main->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
                        </label>
                      </div>
                    </div>
                  </fieldset>
                </div>
                <div class="panel-footer">
                  <div class="row">
                    <div class="col-sm-7 col-sm-offset-2">
                      <button class="btn btn-sm btn-primary btn-action" type="submit"><i class="fa fa-save"></i> Simpan</button>
                      <a class="btn btn-sm btn-flat btn-default" href="<?=base_url($menu->controller)?>/kategori/<?=$tahun->id?>"><i class="fa fa-close"></i> Batal</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
<script>
  var faIcon = {
    valid: 'fa fa-check-circle fa-lg text-success',
    invalid: 'fa fa-times-circle fa-lg',
    validating: 'fa fa-refresh'
  }
  $('#form').bootstrapValidator({
    live: 'enabled',
		message: 'Nilai tidak valid.',
		feedbackIcons: faIcon,
		fields: {

    }
	}).on('success.field.bv', function(e, data) {
		var $parent = data.element.parents('.form-group');
		$parent.removeClass('has-success');
	}).on('success.form.bv', function(e) {
    $('.btn-action').html('<i class="fa fa-spin fa-spinner"></i> Proses...');
  }).on('status.field.bv', function(e, data) {
    if (data.bv.getSubmitButton()) {
      data.bv.disableSubmitButtons(false);
    }
  });
</script>