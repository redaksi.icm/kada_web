<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dt_statistik extends MY_Controller
{

	var $access, $menu, $group_id, $page;

	function __construct()
	{
		parent::__construct();

		$controller = 'dt_statistik';

		if ($this->session->userdata('controller') != $controller) {
			$this->session->unset_userdata('search');
			$this->session->unset_userdata('order');
			$this->session->unset_userdata('per_page');
			$this->session->set_userdata(array('controller' => $controller));
		}
		$this->load->model('ap_config/m_ap_config');
		$this->group_id = $this->session->userdata('group_id');
		$this->menu = $this->m_ap_config->get_menu($this->group_id, $controller);
		$this->access = $this->m_ap_config->get_access($this->group_id, $controller);

		if ($this->menu == null) {
			redirect(base_url() . 'ap_error/error_403');
		}

		$this->load->model('m_dt_statistik');
	}

	public function index()
	{
		redirect(base_url($this->menu->controller) . '/tahun');
	}

	public function tahun()
	{
		if (in_array('read', $this->access)) {
			$data['access'] = $this->access;
			$data['title'] = 'Manajemen';
			$data['menu'] = $this->menu;
			$data['main'] = $this->m_dt_statistik->tahun_get_all();

			// create log
			create_log('read', $this->menu->menu);
			$this->render('tahun', $data);
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function tahun_form($id = null)
	{
		$data['access'] = $this->access;
		$data['menu'] = $this->menu;

		if ($id == null) {
			if (in_array('create', $this->access)) {
				$data['title'] = 'Tambah';
				$data['action'] = 'tahun_create';
				$data['main'] = null;
				$this->render('tahun_form', $data);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			if (in_array('update', $this->access)) {
				$data['title'] = 'Ubah';
				$data['action'] = 'tahun_update';
				$data['main'] = $this->m_dt_statistik->tahun_get_id($id);
				$this->render('tahun_form', $data);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		}
	}

	public function tahun_create()
	{
		if (in_array('create', $this->access)) {
			$data = $this->input->post(null, true);
			if ($data != null) {
				$data['id'] = md5(date('YmdHis'));
				$data['created'] = date('Y-m-d H:i:s');
				$data['created_by'] = $this->session->userdata('fullname');
				if (!isset($data['is_active'])) {
					$data['is_active'] = 0;
				}
				$this->m_dt_statistik->tahun_create($data);
				create_log('create', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil ditambahkan.');
				redirect(base_url($this->menu->controller) . '/tahun/');
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function tahun_update()
	{
		if (in_array('update', $this->access)) {
			$data = $this->input->post(null, true);
			if ($data != null) {
				$data['updated'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('fullname');
				if (!isset($data['is_active'])) {
					$data['is_active'] = 0;
				}
				$id = $data['id'];
				$this->m_dt_statistik->tahun_update($id, $data);
				create_log('update', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil diubah.');
				//redirect
				$page = $this->session->userdata('page');
				redirect(base_url($this->menu->controller) . '/tahun/');
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function tahun_delete($id = null)
	{
		if (in_array('delete', $this->access)) {
			if ($id != null) {
				$this->m_dt_statistik->tahun_delete($id);
				create_log('delete', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil dihapus!');
				redirect(base_url($this->menu->controller) . '/tahun');
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	// =======================================================================================
	// KATEGORI
	// =======================================================================================

	public function kategori($tahun_id)
	{
		if (in_array('read', $this->access)) {
			$data['access'] = $this->access;
			$data['menu'] = $this->menu;
			$data['main'] = $this->m_dt_statistik->kategori_get_all($tahun_id);
			$data['tahun'] = $this->m_dt_statistik->tahun_get_id($tahun_id);
			// create log
			create_log('read', $this->menu->menu);
			$this->render('kategori', $data);
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function kategori_form($tahun_id = null, $id = null)
	{
		$data['access'] = $this->access;
		$data['menu'] = $this->menu;
		$data['tahun'] = $this->m_dt_statistik->tahun_get_id($tahun_id);

		if ($id == null) {
			if (in_array('create', $this->access)) {
				$data['title'] = 'Tambah Kategori';
				$data['action'] = 'kategori_create';
				$data['main'] = null;
				$this->render('kategori_form', $data);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			if (in_array('update', $this->access)) {
				$data['title'] = 'Ubah';
				$data['action'] = 'kategori_update';
				$data['main'] = $this->m_dt_statistik->kategori_get_id($tahun_id, $id);
				$this->render('kategori_form', $data);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		}
	}

	public function kategori_create($tahun_id)
	{
		if (in_array('create', $this->access)) {
			$data = $this->input->post(null, true);
			if ($data != null) {
				$data['id'] = md5(date('YmdHis'));
				$data['created'] = date('Y-m-d H:i:s');
				$data['created_by'] = $this->session->userdata('fullname');
				if (!isset($data['is_active'])) {
					$data['is_active'] = 0;
				}
				$this->m_dt_statistik->kategori_create($data);
				create_log('create', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil ditambahkan.');
				redirect(base_url($this->menu->controller) . '/kategori/' . $tahun_id);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function kategori_update($tahun_id)
	{
		if (in_array('update', $this->access)) {
			$data = $this->input->post(null, true);
			if ($data != null) {
				$data['updated'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('fullname');
				if (!isset($data['is_active'])) {
					$data['is_active'] = 0;
				}
				$id = $data['id'];
				$this->m_dt_statistik->kategori_update($id, $data);
				create_log('update', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil diubah.');
				//redirect
				$page = $this->session->userdata('page');
				redirect(base_url($this->menu->controller) . '/kategori/' . $tahun_id);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function kategori_delete($tahun_id, $id = null)
	{
		if (in_array('delete', $this->access)) {
			if ($id != null) {
				$this->m_dt_statistik->kategori_delete($id);
				create_log('delete', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil dihapus!');
				redirect(base_url($this->menu->controller) . '/kategori/' . $tahun_id);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	// =======================================================================================
	// SUB KATEGORI
	// =======================================================================================

	public function subkategori($tahun_id, $kategori_id)
	{
		if (in_array('read', $this->access)) {
			$data['access'] = $this->access;
			$data['menu'] = $this->menu;
			$data['main'] = $this->m_dt_statistik->subkategori_get_all($tahun_id, $kategori_id);
			$data['tahun'] = $this->m_dt_statistik->tahun_get_id($tahun_id);
			$data['kategori'] = $this->m_dt_statistik->kategori_get_id($tahun_id, $kategori_id);
			// create log
			create_log('read', $this->menu->menu);
			$this->render('subkategori', $data);
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function subkategori_form($tahun_id = null, $kategori_id = null, $id = null)
	{
		$data['access'] = $this->access;
		$data['menu'] = $this->menu;
		$data['tahun'] = $this->m_dt_statistik->tahun_get_id($tahun_id);
		$data['kategori'] = $this->m_dt_statistik->kategori_get_id($tahun_id, $kategori_id);

		if ($id == null) {
			if (in_array('create', $this->access)) {
				$data['title'] = 'Tambah Kategori';
				$data['action'] = 'subkategori_create';
				$data['main'] = null;
				$this->render('subkategori_form', $data);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			if (in_array('update', $this->access)) {
				$data['title'] = 'Ubah';
				$data['action'] = 'subkategori_update';
				$data['main'] = $this->m_dt_statistik->subkategori_get_id($tahun_id, $kategori_id, $id);
				$this->render('subkategori_form', $data);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		}
	}

	public function subkategori_create($tahun_id, $kategori_id)
	{
		if (in_array('create', $this->access)) {
			$data = $this->input->post(null, true);
			if ($data != null) {
				$data['id'] = md5(date('YmdHis'));
				$data['created'] = date('Y-m-d H:i:s');
				$data['created_by'] = $this->session->userdata('fullname');
				if($data['jenis'] == 0){
					$data['berkas'] = $this->_uploadBerkas();
				}
				unset($data['old_berkas']);
				if (!isset($data['is_active'])) {
					$data['is_active'] = 0;
				}
				$this->m_dt_statistik->subkategori_create($data);
				create_log('create', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil ditambahkan.');
				redirect(base_url($this->menu->controller) . '/subkategori/' . $tahun_id . '/' . $kategori_id);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function subkategori_update($tahun_id, $kategori_id)
	{
		if (in_array('update', $this->access)) {
			$data = $this->input->post(null, true);
			if ($data != null) {
				$data['updated'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('fullname');
				if (!isset($data['is_active'])) {
					$data['is_active'] = 0;
				}
				$id = $data['id'];
				if (!empty($_FILES["berkas"]["name"])) {
					$data['berkas'] = $this->_uploadBerkas();
				} else {
					$data['berkas'] = $data["old_berkas"];
				}
				unset($data['old_berkas']);
				$this->m_dt_statistik->subkategori_update($id, $data);
				create_log('update', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil diubah.');
				//redirect
				$page = $this->session->userdata('page');
				redirect(base_url($this->menu->controller) . '/subkategori/' . $tahun_id . '/' . $kategori_id);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	public function subkategori_delete($tahun_id, $kategori_id, $id = null)
	{
		if (in_array('delete', $this->access)) {
			if ($id != null) {
				$this->m_dt_statistik->subkategori_delete($id);
				create_log('delete', $this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil dihapus!');
				redirect(base_url($this->menu->controller) . '/subkategori/' . $tahun_id . '/' . $kategori_id);
			} else {
				redirect(base_url() . 'ap_error/error_403');
			}
		} else {
			redirect(base_url() . 'ap_error/error_403');
		}
	}

	private function _uploadBerkas()
	{
		$config['upload_path']          = FCPATH . '/berkas/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg|pdf';
		$config['remove_spaces'] 				= TRUE;
		$config['encrypt_name'] 				= TRUE;
		$config['overwrite']						= TRUE;
		// $config['max_size']             = 25;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('berkas')) {
			return $this->upload->data("file_name");
		} else {
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
			exit();
		}

		return "no-image.jpeg";
	}
}
