<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_statistik extends CI_Model {

	public function tahun_get_all()
  {
    return $this->db->query(
      "SELECT a.*, 
        IF(b.jumlah IS NULL, 0, b.jumlah) as jumlah
      FROM dt_tahun a
      LEFT JOIN (
        SELECT tahun_id, COUNT(1) as jumlah FROM dt_kategori GROUP BY tahun_id
      ) as b ON b.tahun_id = a.id
      WHERE 
        a.is_deleted = 0"
    )->result();
  }

  public function tahun_get_id($id)
  {
    return $this->db->query(
      "SELECT * FROM dt_tahun 
      WHERE 
        id = '$id'"
    )->row();
  }

  public function tahun_create($data)
  {
    $this->db->insert('dt_tahun',$data);
  }

  public function tahun_update($id,$data)
  {
    $this->db->where('id',$id)->update('dt_tahun',$data);
  }

  public function tahun_delete($id)
  {
    $this->db->where('id',$id)->delete('dt_tahun');
  }

  // =====================================================================
  // KATEGORI
  // =====================================================================

  public function kategori_get_all($tahun_id)
  {
    return $this->db->query(
      "SELECT a.*,b.tahun,IF(c.jumlah IS NULL, 0, c.jumlah) as jumlah FROM dt_kategori a
      LEFT JOIN dt_tahun b ON a.tahun_id = b.id 
      LEFT JOIN (
        SELECT kategori_id, COUNT(1) as jumlah FROM dt_subkategori GROUP BY kategori_id
      ) as c ON c.kategori_id = a.id
      WHERE 
        a.tahun_id = '$tahun_id' AND 
        a.is_deleted = 0"
    )->result();
  }

  public function kategori_get_id($tahun_id, $id)
  {
    return $this->db->query(
      "SELECT a.*,b.tahun FROM dt_kategori a
      LEFT JOIN dt_tahun b ON a.tahun_id = b.id
      WHERE 
        a.tahun_id = '$tahun_id' AND a.id='$id'"
    )->row();
  }

  public function kategori_create($data)
  {
    $this->db->insert('dt_kategori',$data);
  }

  public function kategori_update($id,$data)
  {
    $this->db->where('id',$id)->update('dt_kategori',$data);
  }

  public function kategori_delete($id)
  {
    $this->db->where('id',$id)->delete('dt_kategori');
  }

  // =====================================================================
  // SUB KATEGORI
  // =====================================================================

  public function subkategori_get_all($tahun_id, $kategori_id)
  {
    return $this->db->query(
      "SELECT a.*, b.tahun, c.kategori FROM dt_subkategori a
      LEFT JOIN dt_tahun b ON a.tahun_id = b.id 
      LEFT JOIN dt_kategori c ON a.kategori_id = c.id 
      WHERE 
        a.tahun_id = '$tahun_id' AND a.kategori_id = '$kategori_id' AND 
        a.is_deleted = 0"
    )->result();
  }

  public function subkategori_get_id($tahun_id, $kategori_id, $id)
  {
    return $this->db->query(
      "SELECT a.*,b.tahun FROM dt_subkategori a
      LEFT JOIN dt_tahun b ON a.tahun_id = b.id
      WHERE 
        a.tahun_id = '$tahun_id' AND a.id='$id'"
    )->row();
  }

  public function subkategori_create($data)
  {
    $this->db->insert('dt_subkategori',$data);
  }

  public function subkategori_update($id,$data)
  {
    $this->db->where('id',$id)->update('dt_subkategori',$data);
  }

  public function subkategori_delete($id)
  {
    $this->db->where('id',$id)->delete('dt_subkategori');
  }

  
}
