<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Statistik extends MX_Controller
{

  public function tahun_get()
  {
    $d = $this->db->query(
      "SELECT 
        b.id, b.tahun, 
        COUNT(a.id) AS jml_kategori
      FROM dt_kategori a
      RIGHT JOIN dt_tahun b ON a.tahun_id = b.id
      GROUP BY a.tahun_id
      ORDER BY b.tahun DESC"
    )->result();

    $res = array(
      "code" => 200,
      "message" => "Data found",
      "data" => $d
    );

    header('Content-Type: application/json');
    echo json_encode($res);
  }

  public function kategori_get($tahun_id)
  {
    $d = $this->db->query(
      "SELECT 
        b.id, b.kategori, 
        COUNT(a.id) AS jml_subkategori
      FROM dt_subkategori a
      RIGHT JOIN dt_kategori b ON a.kategori_id = b.id 
      WHERE b.tahun_id = '$tahun_id'
      GROUP BY a.kategori_id
      ORDER BY b.created ASC"
    )->result();

    $res = array(
      "code" => 200,
      "message" => "Data found",
      "data" => $d
    );

    header('Content-Type: application/json');
    echo json_encode($res);
  }

  public function subkategori_get($kategori_id)
  {
    $d = $this->db->query(
      "SELECT id,subkategori,jenis,berkas,url FROM dt_subkategori a WHERE a.kategori_id = '$kategori_id'"
    )->result_array();

    $res = array();
    foreach ($d as $k => $v) {
      $res[$k] = $v;
      $res[$k]['berkas'] = base_url() . 'berkas/' . $v['berkas'];
    }

    $res = array(
      "code" => 200,
      "message" => "Data found",
      "data" => $d
    );

    header('Content-Type: application/json');
    echo json_encode($res);
  }
}
