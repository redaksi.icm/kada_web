<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Indikator extends MX_Controller
{

  public function indikator_get()
  {
    $result = array();
    // inflasi
    $res = array();
    $inflasi = $this->db->query("SELECT * FROM dt_inflasi ORDER BY tahun DESC")->row();
    $res['id'] = 'inflasi';
    $res['images'] = base_url() . 'berkas/inflasi.png';
    $res['indikator'] = 'Inflasi';
    if ($inflasi == null) {
      $res['tahun'] = date('Y');
      $res['nilai'] = '0';
    } else {
      $res['tahun'] = $inflasi->tahun;
      $res['nilai'] = $inflasi->nilai;
    }
    array_push($result, $res);

    // kemiskinan
    $res = array();
    $kemiskinan = $this->db->query("SELECT * FROM dt_kemiskinan ORDER BY tahun DESC")->row();
    $res['id'] = 'kemiskinan';
    $res['images'] = base_url() . 'berkas/kemiskinan.png';
    $res['indikator'] = 'Kemiskinan';
    if ($kemiskinan == null) {
      $res['tahun'] = date('Y');
      $res['nilai'] = '0';
    } else {
      $res['tahun'] = $kemiskinan->tahun;
      $res['nilai'] = $kemiskinan->nilai;
    }
    array_push($result, $res);

    // pengangguran
    $res = array();
    $pengangguran = $this->db->query("SELECT * FROM dt_pengangguran ORDER BY tahun DESC")->row();
    $res['id'] = 'pengangguran';
    $res['images'] = base_url() . 'berkas/pengangguran.png';
    $res['indikator'] = 'Pengangguran';
    if ($pengangguran == null) {
      $res['tahun'] = date('Y');
      $res['nilai'] = '0';
    } else {
      $res['tahun'] = $pengangguran->tahun;
      $res['nilai'] = $pengangguran->nilai;
    }
    array_push($result, $res);

    // ipm
    $res = array();
    $ipm = $this->db->query("SELECT * FROM dt_ipm ORDER BY tahun DESC")->row();
    $res['id'] = 'ipm';
    $res['images'] = base_url() . 'berkas/ipm.png';
    $res['indikator'] = 'IPM';
    if ($ipm == null) {
      $res['tahun'] = date('Y');
      $res['nilai'] = '0';
    } else {
      $res['tahun'] = $ipm->tahun;
      $res['nilai'] = $ipm->nilai;
    }
    array_push($result, $res);

    // ekonomi
    $res = array();
    $ekonomi = $this->db->query("SELECT * FROM dt_ekonomi ORDER BY tahun DESC")->row();
    $res['id'] = 'ekonomi';
    $res['images'] = base_url() . 'berkas/ekonomi.png';
    $res['indikator'] = 'Pertumbuhan ekonomi';
    if ($ekonomi == null) {
      $res['tahun'] = date('Y');
      $res['nilai'] = '0';
    } else {
      $res['tahun'] = $ekonomi->tahun;
      $res['nilai'] = $ekonomi->nilai;
    }
    array_push($result, $res);

    // penduduk
    $res = array();
    $penduduk = $this->db->query("SELECT * FROM dt_penduduk ORDER BY tahun DESC")->row();
    $res['id'] = 'penduduk';
    $res['images'] = base_url() . 'berkas/penduduk.png';
    $res['indikator'] = 'Penduduk';
    if ($penduduk == null) {
      $res['tahun'] = date('Y');
      $res['nilai'] = '0';
    } else {
      $res['tahun'] = $penduduk->tahun;
      $res['nilai'] = $penduduk->nilai;
    }
    array_push($result, $res);

    $r = array(
      "code" => 200,
      "message" => "Data found",
      "data" => $result
    );

    header('Content-Type: application/json');
    echo json_encode($r);
  }

  public function detail_get($id = null)
  {
    switch ($id) {
      case 'inflasi':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_inflasi ORDER BY tahun ASC")->result();
        break;

      case 'kemiskinan':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_kemiskinan ORDER BY tahun ASC")->result();
        break;

      case 'pengangguran':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_pengangguran ORDER BY tahun ASC")->result();
        break;

      case 'ipm':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_ipm ORDER BY tahun ASC")->result();
        break;

      case 'ekonomi':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_ekonomi ORDER BY tahun ASC")->result();
        break;

      case 'penduduk':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_penduduk ORDER BY tahun ASC")->result();
        break;

      default:
        $d = $this->db->query("SELECT tahun, nilai FROM dt_inflasi")->result();
        break;
    }

    $r = array(
      "code" => 200,
      "message" => "Data found",
      "data" => $d
    );

    header('Content-Type: application/json');
    echo json_encode($r);
  }

  public function grafik_get($id = null)
  {
    switch ($id) {
      case 'inflasi':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_inflasi")->result();
        break;

      case 'kemiskinan':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_kemiskinan")->result();
        break;

      case 'pengangguran':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_pengangguran")->result();
        break;

      case 'ipm':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_ipm")->result();
        break;

      case 'ekonomi':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_ekonomi")->result();
        break;

      case 'penduduk':
        $d = $this->db->query("SELECT tahun, nilai FROM dt_penduduk")->result();
        break;

      default:
        $d = $this->db->query("SELECT tahun, nilai FROM dt_inflasi")->result();
        break;
    }
    $res = array();
    foreach ($d as $row) {
      array_push($res, intval($row->nilai));
    }
    // foreach ($d as $row) {
    //   $r = array();
    //   $r['tahun'] = intval($row->tahun);
    //   $r['nilai'] = intval($row->nilai);
    //   array_push($res, $r);
    // }
    echo json_encode(array('data' => $res));
  }
}
