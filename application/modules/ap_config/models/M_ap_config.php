<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_config extends CI_Model {

	public function get_profile()
  {
    return $this->db->query("SELECT * FROM ap_profile")->row();
  }

  public function get_first_menu($group_id)
  {
    $query = $this->db->query(
      "SELECT * FROM ap_group_menu a
      JOIN ap_menu b ON a.menu_id = b.id
      WHERE a.group_id = '$group_id' 
        AND b.type = 2 AND b.is_active = 1
      ORDER BY a.menu_id ASC"
    );

    return $query->row();
  }

  public function get_group_menu($parent = null)
  {
    $group_id = $this->session->userdata('group_id');
    
    $sql_where = '';
    $sql_where .= ($parent != '') ? "b.parent = '$parent'" : 'b.parent = ""';

    $query = $this->db->query(
      "SELECT * 
      FROM
        ap_group_menu a
      JOIN ap_menu b ON a.menu_id = b.id
      WHERE
        a.group_id = '$group_id' AND 
        $sql_where AND b.is_active = 1
      ORDER BY a.menu_id"
    );
    if ($query->num_rows() > 0) {
      $result = $query->result_array();
      foreach ($result as $key => $val) {
        $result[$key]['child'] = $this->get_group_menu($result[$key]['menu_id']);
      }
      return $result;
    }else{
      return array();
    }
  }

  public function get_menu($group_id,$controller)
  {
    $query = $this->db->query(
      "SELECT a.group_id,b.* FROM ap_group_menu a 
      JOIN ap_menu b ON a.menu_id = b.id
      WHERE
        a.group_id = '$group_id' AND 
        b.controller = '$controller'"
    );

    return $query->row();
  }
  
  public function get_access($group_id,$controller)
  {
    $res = array();
    $query = $this->db->query(
      "SELECT a.role FROM ap_group_role a
      JOIN ap_menu b ON a.menu_id = b.id
      WHERE 
        b.controller = '$controller' AND
        a.group_id = '$group_id'
      "
    );

    foreach ($query->result() as $row) {
      array_push($res,$row->role);
    }
    
    return $res;
  }
  
}
