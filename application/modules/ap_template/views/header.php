<!DOCTYPE html>
<html lang="id">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=@$profile->singkatan?> - <?=@$profile->aplikasi?></title>
    <link rel="shortcut icon" href="<?=base_url()?>/img/<?=@$profile->logo?>" type="image/x-icon">
    <!--STYLESHEET-->
    <!--=================================================-->
    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <!--Jasmine Stylesheet [ REQUIRED ]-->
    <link href="<?=base_url()?>css/style.css" rel="stylesheet">
    <!--Font Awesome [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Sweetalert2 -->
    <link href="<?=base_url()?>plugins/sweetalert2/dist/sweetalert2.css" rel="stylesheet"/>
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/bootstrap-validator/bootstrapValidator.min.css" rel="stylesheet">
    <!--Bootstrap Select [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/bootstrap-select/bootstrap-select.css" rel="stylesheet">
    <!--Select2 [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?=base_url()?>plugins/select2/css/select2-bootstrap.css" rel="stylesheet">
    <!--Bootstrap Timepicker [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <!--Bootstrap Datepicker [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">
    <!-- Data Table -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>plugins/DataTables/datatables.min.css"/>

    <!--SCRIPT-->
    <!--=================================================-->
    <!--Page Load Progress Bar [ OPTIONAL ]-->
    <link href="<?=base_url()?>plugins/pace/pace.min.css" rel="stylesheet">
    <script src="<?=base_url()?>plugins/pace/pace.min.js"></script>
    <!--jQuery [ REQUIRED ]-->
    <script src="<?=base_url()?>js/jquery-2.1.1.min.js"></script>
    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?=base_url()?>js/bootstrap.min.js"></script>
    <!-- Sweetalert2 -->
    <script src="<?=base_url()?>plugins/sweetalert2/dist/sweetalert2.js"></script>
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <script src="<?=base_url()?>plugins/bootstrap-validator/bootstrapValidator.min.js"></script>
    <script src="<?=base_url()?>plugins/bootstrap-validator/id_ID.js"></script>
    <!--Select2 [ OPTIONAL ]-->
    <script src="<?=base_url()?>plugins/select2/js/select2.min.js"></script>
    <!--Bootstrap Select [ OPTIONAL ]-->
    <script src="<?=base_url()?>plugins/bootstrap-select/bootstrap-select.js"></script>
    <!-- Jquery Chained -->
    <script src="<?=base_url()?>plugins/jquery_chained/jquery.chained.min.js"></script>
    <script src="<?=base_url()?>plugins/jquery_chained/jquery.chained.remote.min.js"></script>
    <!-- Autonumeric -->
    <script src="<?=base_url()?>plugins/autoNumeric/autoNumeric.js"></script>
    <!--Bootstrap Timepicker [ OPTIONAL ]-->
    <script src="<?=base_url()?>plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <!--Bootstrap Datepicker [ OPTIONAL ]-->
    <script src="<?=base_url()?>plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <!-- Data Table -->
    <script type="text/javascript" src="<?=base_url()?>plugins/DataTables/datatables.min.js"></script>
    <!-- Tanggal -->
    <script src="<?=base_url()?>js/tanggal.js"></script>
    <!-- Helper -->
    <script src="<?=base_url()?>js/helper.js"></script>
    <!-- Custom JS -->
    <script>
      $(document).ready(function () {
        // success message
        const flashData = $('.flash-data').data('flashdata');
        // alert(flashData);
        if (flashData) {
          Swal.fire({
            title : 'Sukses',
            text : flashData,
            type : 'success',
            confirmButtonColor : '#5cb85c'
          });
        }
        // error message
        const errorFlashData = $('.error-flash-data').data('flashdata');
        if (errorFlashData) {
          Swal.fire({
            title : 'Gagal',
            text : errorFlashData,
            type : 'error'
            // confirmButtonColor : '#25a79f'
          });
        }
        //delete confirm
        $('.btn-delete').on('click',function (e) {
          e.preventDefault();

          const href = $(this).attr('href');

          Swal.fire({
            title: 'Apakah Anda yakin?',
            text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#eb3b5a',
            cancelButtonColor: '#b2bec3',
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal'
          }).then((result) => {
            if (result.value) {
              document.location.href = href;
            }
          })
        })
        //choosen
        $('.select2').select2({
          theme: "bootstrap",
          width: '100%'
        });
        $('.select2-multiple').select2({
          theme: "bootstrap",
          width: '100%'
        });
        //autonumeric
        $(".num-ori").autoNumeric('init',{
          aSep: '',
          aDec: '',
          aForm: true,
          vMax: '999999999999',
          vMin: '-999999999999'
        });
        $(".num-int").autoNumeric('init',{
          aSep: '.',
          aDec: ',',
          aForm: true,
          vMax: '999999999999',
          vMin: '-999999999999'
        });
        $(".num-float").autoNumeric('init',{
          aSep: '.',
          aDec: ',',
          aForm: true,
          vMax: '999999999999.99',
          vMin: '-999999999999.99',
          eDec: '0'
        });
        //datepicker
        $(".datepicker").datepicker({
          format: "dd-mm-yyyy",
          autoclose: true,
          clearBtn: true
        });
        //disable input
        $(".input-disabled").keydown(function(event) { 
          return false;
        });
      })

      function checkAll(cb) {
        if (cb.checked) {
          $('.checkitem').prop('checked', true);
          $('.checkitem-label').addClass('active');
        }else{
          $('.checkitem').prop('checked', false);
          $('.checkitem-label').removeClass('active');
        }
      }

      function checkItemAll() {
        console.log($('.checkitem:checked').length+" dari "+$('.checkitem').length);
        if($('.checkitem:checked').length == $('.checkitem').length){
          console.log('check semua');
          $(".checkall").prop('checked', true);
          $('.checkall-label').addClass('active');
        }else{
          $(".checkall").prop('checked', false);
          $('.checkall-label').removeClass('active');
        }
      }
      
      function multipleAction(type) {
        var desc;
        var confirmColor;
        var confirmButton;
        switch (type) {
          case 'enable':
            desc = 'Data ini mungkin terhubung dengan data lain.';
            confirmColor = '#5cb85c';
            confirmButton = 'Aktifkan';
            break;
        
          case 'disable':
            desc = 'Data ini mungkin terhubung dengan data lain.';
            confirmColor = '#d9534f';
            confirmButton = 'Non Aktifkan';
            break;

          case 'delete':
            desc = 'Aksi ini tidak bisa diurungkan. Data ini mungkin terhubung dengan data lain.';
            confirmColor = '#d9534f';
            confirmButton = 'Hapus';
        }
        Swal.fire({
          title: 'Apakah Anda yakin?',
          text: desc,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: confirmColor,
          confirmButtonText: confirmButton,
          cancelButtonColor: '#b2bec3',
          cancelButtonText: 'Batal'
        }).then((result) => {
          if (result.value) {
            $('#form-multiple').attr('action', '<?=base_url($menu->controller)?>/multiple/'+type).submit();
          }
        })
      }
    </script>
  </head>