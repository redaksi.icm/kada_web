<!--MAIN NAVIGATION-->
<nav id="mainnav-container">
  <div id="mainnav">
    <!--Menu-->
    <div id="mainnav-menu-wrap">
      <div class="nano">
        <div class="nano-content">
          <ul id="mainnav-menu" class="list-group">
            <?php foreach($sidenav as $lvl1):?>
              <?php if($lvl1['type'] == 3): ?>
                <li class="list-header"><?=$lvl1['menu']?></li>
              <?php else:?>
                <li class="
                  <?php if($menu->controller == $lvl1['controller']){echo ' active-link';}?>
                  <?php 
                  foreach($lvl1['child'] as $lvl2){
                    if($menu->controller == $lvl2['controller']){
                      echo ' active-link';
                      break;
                    }
                    foreach($lvl2['child'] as $lvl3){
                      if($menu->controller == $lvl3['controller']){
                        echo ' active-link';
                        break;
                      }
                    }
                  } ?>
                  ">
                  <a href="
                    <?php 
                      if(count($lvl1['child']) > 0){
                        echo "javascript:void(0)";
                      }else{
                        echo base_url().$lvl1['controller'].'/'.$lvl1['url'];
                      }
                    ?>
                  ">
                    <i class="fa fa-<?=$lvl1['icon']?>"></i>
                    <span class="menu-title"><?=$lvl1['menu']?></span>
                    <?php if(count($lvl1['child'])>0):?>
                      <i class="arrow"></i>
                    <?php endif; ?>
                  </a>
                  <!--Submenu-->
                  <?php if(count($lvl1['child'])>0):?>  
                    <ul class="collapse <?php 
                      foreach($lvl1['child'] as $lvl2){
                        if($menu->controller == $lvl2['controller']){
                          echo ' in';
                          break;
                        }
                        foreach($lvl2['child'] as $lvl3){
                          if($menu->controller == $lvl3['controller']){
                            echo ' in';
                            break;
                          }
                        }
                      } ?>
                    ">
                      <?php foreach($lvl1['child'] as $lvl2):?>
                        <li class="
                          <?php if($menu->controller == $lvl2['controller']){echo 'active-sub';}?>
                          <?php 
                            foreach($lvl2['child'] as $lvl3){
                              if($menu->controller == $lvl3['controller']){
                                echo ' active';
                                break;
                              }
                            }
                          ?>
                        ">
                          <a href="
                            <?php 
                              if(count($lvl2['child']) > 0){
                                echo "javascript:void(0)";
                              }else{
                                echo base_url().$lvl2['controller'].'/'.$lvl2['url'];
                              }
                            ?>
                          "><i class="fa fa-circle-o"></i> <?=$lvl2['menu']?>
                            <?php if(count($lvl2['child']) > 0): ?>
                              <i class="arrow"></i>
                            <?php endif; ?>
                          </a>
                          <?php if(count($lvl2['child']) > 0): ?>
                            <ul class="collapse 
                              <?php 
                                foreach($lvl2['child'] as $lvl3){
                                  if($menu->controller == $lvl3['controller']){
                                    echo ' in';
                                    break;
                                  }
                                }
                              ?>
                            ">
                              <?php foreach($lvl2['child'] as $lvl3):?>
                                <li class="<?php if($menu->controller == $lvl3['controller']){echo 'active-sub';}?>"><a href="<?=base_url().$lvl3['controller'].'/'.$lvl3['url'];?>"><?=$lvl3['menu']?></a></li>
                              <?php endforeach; ?>
                          </ul>
                          <?php endif; ?>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  <?php endif; ?>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
    <!--End menu-->
  </div>
</nav>
<!--END MAIN NAVIGATION-->