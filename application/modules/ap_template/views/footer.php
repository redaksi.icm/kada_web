
      </div>
      <!-- FOOTER -->
      <footer id="footer">
        <!-- Visible when footer positions are static -->
        <div class="pull-right pad-rgt">V <?=$profile->version?></div>
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <!-- Remove the class name "show-fixed" and "hide-fixed" to make the content always appears. -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <p class="pad-lft">&#0169; 
          <?php if($profile->start_year == date('Y')){echo $profile->start_year;}else{echo $profile->start_year.' - '.date('Y');}?> 
          by <?=$profile->opd?>
        </p>
      </footer>
      <!-- END FOOTER -->
    </div>
    <!-- END OF CONTAINER -->
    <!--JAVASCRIPT-->
    <!--=================================================-->
    <!--Jasmine Admin [ RECOMMENDED ]-->
    <script src="<?=base_url()?>js/scripts.js"></script>
  </body>
</html>