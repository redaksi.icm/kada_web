
  <!--TIPS-->
  <!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
  <body>
    <div id="container" class="effect mainnav-lg">
      <!--NAVBAR-->
      <header id="navbar">
        <div id="navbar-container" class="boxed">
          <!--Brand logo & name-->
          <div class="navbar-header">
            <a href="#" class="navbar-brand">              
              <div class="brand-title">
                <span class="brand-text"><?=$profile->singkatan?></span>
              </div>
            </a>
          </div>
          <!--End brand logo & name-->
          <!--Navbar Dropdown-->
          <div class="navbar-content clearfix">
            <ul class="nav navbar-top-links pull-left">
              <!--Navigation toogle button-->
              <li class="tgl-menu-btn">
                <a class="mainnav-toggle" href="<?=base_url()?>#"> <i class="fa fa-navicon fa-lg"></i> </a>
              </li>
              <!--End Navigation toogle button-->
            </ul>
            <ul class="nav navbar-top-links pull-right">
              <!--User dropdown-->
              <li id="dropdown-user" class="dropdown">
                <a href="<?=base_url()?>#" data-toggle="dropdown" class="dropdown-toggle text-right"> <span class="pull-right"> <img class="img-circle img-user media-object" src="<?=base_url()?>img/user.png" alt="Profile Picture"> </span>
                  <div class="username hidden-xs"><?=$this->session->userdata('fullname')?></div>
                </a>
                <div class="dropdown-menu dropdown-menu-right with-arrow">
                  <!-- User dropdown menu -->
                  <ul class="head-list">
                    <li>
                      <a href="<?=base_url()?>ap_auth/logout/action"> <i class="fa fa-sign-out fa-fw"></i> Logout </a>
                    </li>
                  </ul>
                </div>
              </li>
              <!--End user dropdown-->
            </ul>
            <div class="year-container pull-right text-info" style="display:inline-block;height:50px;font-size:12px;padding:17px 5px;color:white;">
              <span id="showDayOne"></span> 
              <span id="showDateOne"></span> 
              <span id="showTimeOne"></span>
            </div>
          </div>
          <!--End Navbar Dropdown-->
        </div>
      </header>
      <!--END NAVBAR-->
      
      <div class="boxed">
        