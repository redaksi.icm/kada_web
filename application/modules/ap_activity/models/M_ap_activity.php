<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_activity extends CI_Model {

	public function get_list($number,$offset = 0,$search = null)
  {    
    if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          if ($key == 'created') {
            $this->db->like($key,$val);
          }else{
            $this->db->where($key,$val);
          }
        }
      }
    }else{
      $this->db->like('created',date('Y-m-d'));
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id','DESC')
      ->get('ap_activity',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          if ($key == 'created') {
            $this->db->like($key,$val);
          }else{
            $this->db->where($key,$val);
          }
        }
      }
    }else{
      $this->db->like('created',date('Y-m-d'));
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id','DESC')
      ->get('ap_activity')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id','DESC')
      ->get('ap_activity')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ap_activity')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('ap_activity')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('ap_activity')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('ap_activity')->row();
  }

  public function create($data)
  {
    $this->db->insert('ap_activity',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('ap_activity',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id',$id)->update('ap_activity',array('is_deleted' => '1'));
  }

  public function delete_permanent()
  {
    $this->db->where('id',$id)->delete('ap_activity');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_activity");
  }

}
