<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_user extends CI_Model {

	public function get_list($number,$offset,$search = null, $order = null)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.fullname LIKE '%".$search['term']."%' ";
        $where .= "OR b.group LIKE '%".$search['term']."%' ";
      }
    }

    if ($order == null) {
      $order['order_field'] = 'created';
      $order['order_type'] = 'asc';
      $this->session->set_userdata(array('order' => $order));
    }
    
    return $this->db->query(
      "SELECT a.*, b.group FROM ap_user a
      LEFT JOIN ap_group b ON a.group_id = b.id 
      $where
      ORDER BY "
        .$order['order_field']." ".$order['order_type'].
      " LIMIT 
        $offset, $number"
    );
  }

  function num_rows($search = null){
		$where = "WHERE a.is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.fullname LIKE '%".$search['term']."%' ";
        $where .= "OR b.group LIKE '%".$search['term']."%' ";
      }
    }
    
    return $this->db->query(
      "SELECT a.*, b.group FROM ap_user a
      LEFT JOIN ap_group b ON a.group_id = b.id 
      $where"
    )->num_rows();
  }
  
  function num_rows_total($search = null){
    return $this->db->query(
      "SELECT a.*, b.group FROM ap_user a
      LEFT JOIN ap_group b ON a.group_id = b.id 
      WHERE a.is_deleted = 0"
    )->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('a.is_deleted',0)
      ->where('is_active',1)
      ->order_by('created','asc')
			->get('ap_user a')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('ap_user')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('ap_user')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('ap_user')->row();
  }

  public function create($data)
  {
    $this->db->insert('ap_user',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('ap_user',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id',$id)->update('ap_user',array('a.is_deleted' => 1));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('ap_user');
  }

  public function enable($id)
  {
    $this->db->where('id',$id)->update('ap_user',array('is_active' => 1));
  }

  public function disable($id)
  {
    $this->db->where('id',$id)->update('ap_user',array('is_active' => 0));
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_user");
  }

}
