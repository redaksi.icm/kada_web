<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i> <?=@$title?> <?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Pengaturan
        </li>
        <li class="active">
          Master Pengguna
        </li>
        <li class="active">
          <a href="<?=base_url($menu->controller)?>">
            <?=$menu->menu?>
          </a>
        </li>
        <li>
          <?php if($action == 'create'):?>
            Tambah
          <?php else: ?>
            Ubah
          <?php endif; ?>
        </li>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Form <?=@$menu->menu?></h3>
      </div>
      <form id="form" class="form-horizontal" action="<?=base_url().$menu->controller?>/<?=$action?>" method="post" autocomplete="off">
        <input type="hidden" name="id" value="<?=@$main->id?>">
        <div class="panel-body">
          <fieldset>
            <div class="form-group">
              <label class="col-lg-2 control-label">Grup <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <select class="select2 form-control input-sm" name="group_id" id="group_id">
                  <?php foreach($group_list as $row):?>
                    <option value="<?=@$row->id?>" <?php if(@$main->group_id == $row->id){echo 'selected';} ?>><?=$row->group?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Nama Pengguna <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="user_name" id="user_name" value="<?=@$main->user_name?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Nama Lengkap <span class="text-danger">*</span></label>
              <div class="col-lg-3">
                <input type="text" class="form-control input-sm" name="fullname" id="fullname" value="<?=@$main->fullname?>" required>
              </div>
            </div>
            <?php if($action == 'create'):?>
              <div class="form-group">
                <label class="col-lg-2 control-label">Kata Sandi <span class="text-danger">*</span></label>
                <div class="col-lg-3">
                  <input type="password" class="form-control input-sm" name="user_password" id="user_password" value="<?=@$main->user_password?>" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Konfirmasi Kata Sandi <span class="text-danger">*</span></label>
                <div class="col-lg-3">
                  <input type="password" class="form-control input-sm" name="confirm_password" id="confirm_password" value="<?=@$main->password?>" required>
                </div>
              </div>
            <?php endif; ?>
            <div class="form-group">
              <label class="col-lg-2 control-label">Aktif</label>
              <div class="col-lg-5">
                <label class="form-checkbox form-icon <?php if($main){if($main->is_active == 1){echo 'active';}}else{echo 'active';}?> form-text">
                  <input type="checkbox" name="is_active" value="1" <?php if($main){if($main->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
                </label>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-sm-7 col-sm-offset-2">
              <button class="btn btn-sm btn-primary btn-action" type="submit"><i class="fa fa-save"></i> Simpan</button>
              <a class="btn btn-sm btn-flat btn-default" href="<?=base_url($menu->controller)?>/index"><i class="fa fa-close"></i> Batal</a>
            </div>
          </div>
        </div>
      </form>
    </div>
    <?php if($action == 'update'):?>
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Form Ganti Kata Sandi <?=@$menu->menu?></h3>
        </div>
        <form id="form_change_password" class="form-horizontal" action="<?=base_url().$menu->controller?>/change_password" method="post" autocomplete="off">
        <input type="hidden" name="id" value="<?=@$main->id?>">
        <div class="panel-body">
          <fieldset>
            <div class="form-group">
              <label class="col-lg-2 control-label">Kata Sandi <span class="text-danger">*</span></label>
              <div class="col-lg-3">
                <input type="password" class="form-control input-sm" name="user_password" id="user_password" value="" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Konfirmasi Kata Sandi <span class="text-danger">*</span></label>
              <div class="col-lg-3">
                <input type="password" class="form-control input-sm" name="confirm_password" id="confirm_password" value="" required>
              </div>
            </div>
          </fieldset>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-sm-7 col-sm-offset-2">
              <button class="btn btn-sm btn-primary btn-action-change-password" type="submit"><i class="fa fa-save"></i> Simpan</button>
              <a class="btn btn-sm btn-flat btn-default" href="<?=base_url($menu->controller)?>/index"><i class="fa fa-close"></i> Batal</a>
            </div>
          </div>
        </div>
      </form>
      </div>
    <?php endif; ?>
  </div>
  <!-- End Page content -->
</div>
<!--END CONTENT CONTAINER-->
<script>
  var faIcon = {
    valid: 'fa fa-check-circle fa-lg text-success',
    invalid: 'fa fa-times-circle fa-lg',
    validating: 'fa fa-refresh'
  }
  $('#form').bootstrapValidator({
    live: 'enabled',
		message: 'Nilai tidak valid.',
		feedbackIcons: faIcon,
		fields: {
      user_password: {
        validators: {
          notEmpty: {
            message: 'Kata sandi tidak boleh kosong'
          },
          identical: {
            field: 'confirm_password',
            message: 'Kata sandi tidak sama'
          }
        }
      },
      confirm_password: {
        validators: {
          notEmpty: {
            message: 'Konfirmasi kata sandi tidak boleh kosong'
          },
          identical: {
            field: 'password',
            message: 'Kata sandi tidak sama'
          }
        }
      }
    }
	}).on('success.field.bv', function(e, data) {
		// $(e.target)  --> The field element
		// data.bv      --> The BootstrapValidator instance
		// data.field   --> The field name
		// data.element --> The field element

		var $parent = data.element.parents('.form-group');

		// Remove the has-success class
		$parent.removeClass('has-success');
	}).on('success.form.bv', function(e) {
    // Make button action proses
    $('.btn-action').html('<i class="fa fa-spin fa-spinner"></i> Proses...');
  });
  $('#form_change_password').bootstrapValidator({
    live: 'enabled',
		message: 'Nilai tidak valid.',
		feedbackIcons: faIcon,
		fields: {
      user_password: {
        validators: {
          notEmpty: {
            message: 'Kata sandi tidak boleh kosong'
          },
          identical: {
            field: 'confirm_password',
            message: 'Kata sandi tidak sama'
          }
        }
      },
      confirm_password: {
        validators: {
          notEmpty: {
            message: 'Konfirmasi kata sandi tidak boleh kosong'
          },
          identical: {
            field: 'password',
            message: 'Kata sandi tidak sama'
          }
        }
      }
    }
	}).on('success.field.bv', function(e, data) {
		// $(e.target)  --> The field element
		// data.bv      --> The BootstrapValidator instance
		// data.field   --> The field name
		// data.element --> The field element

		var $parent = data.element.parents('.form-group');

		// Remove the has-success class
		$parent.removeClass('has-success');
	}).on('success.form.bv', function(e) {
    // Make button action proses
    $('.btn-action-change-password').html('<i class="fa fa-spin fa-spinner"></i> Proses...');
  });
</script>