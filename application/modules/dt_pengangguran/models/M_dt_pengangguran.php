<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_pengangguran extends CI_Model {

	public function get_list($number,$offset,$search = null, $order = null)
  {
    $where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.role LIKE '%".$search['term']."%' ";
        $where .= "OR a.description LIKE '%".$search['term']."%' ";
      }
    }

    if ($order == null) {
      $order['order_field'] = 'tahun';
      $order['order_type'] = 'desc';
      $this->session->set_userdata(array('order' => $order));
    }
    
    return $this->db->query(
      "SELECT * FROM dt_pengangguran a 
      $where
      ORDER BY "
        .$order['order_field']." ".$order['order_type'].
      " LIMIT 
        $offset, $number"
    );
  }

  function num_rows($search = null){
		$where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.role LIKE '%".$search['term']."%' ";
        $where .= "OR a.description LIKE '%".$search['term']."%' ";
      }
    }
    
    return $this->db->query(
      "SELECT * FROM dt_pengangguran a 
      $where"
    )->num_rows();
  }
  
  function num_rows_total($search = null){
    return $this->db->query(
      "SELECT * FROM dt_pengangguran a 
      WHERE is_deleted = 0"
    )->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted',0)
      ->where('is_active',1)
      ->order_by('created','asc')
			->get('dt_pengangguran')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('dt_pengangguran')->row();
  }

  public function get_by_tahun($tahun)
  {
    return $this->db->where('tahun',$tahun)->get('dt_pengangguran')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('dt_pengangguran')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('dt_pengangguran')->row();
  }

  public function create($data)
  {
    $this->db->insert('dt_pengangguran',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('dt_pengangguran',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id',$id)->update('dt_pengangguran',array('is_deleted' => 1));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('dt_pengangguran');
  }

  public function enable($id)
  {
    $this->db->where('id',$id)->update('dt_pengangguran',array('is_active' => 1));
  }

  public function disable($id)
  {
    $this->db->where('id',$id)->update('dt_pengangguran',array('is_active' => 0));
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_pengangguran");
  }

}
