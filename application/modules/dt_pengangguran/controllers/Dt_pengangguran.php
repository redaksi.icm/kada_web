<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dt_pengangguran extends MY_Controller {

	var $access, $menu, $group_id, $page;

  function __construct(){
		parent::__construct();

		$controller = 'dt_pengangguran';

    if($this->session->userdata('controller') != $controller){
      $this->session->unset_userdata('search');
      $this->session->unset_userdata('order');
      $this->session->unset_userdata('per_page');
      $this->session->set_userdata(array('controller' => $controller));
		}
    $this->load->model('ap_config/m_ap_config');
		$this->group_id = $this->session->userdata('group_id');
		$this->menu = $this->m_ap_config->get_menu($this->group_id,$controller);
		$this->access = $this->m_ap_config->get_access($this->group_id,$controller);
		
		if ($this->menu == null) {
			redirect(base_url().'ap_error/error_403');
		}

		$this->load->model('m_dt_pengangguran');
		$this->load->model('ap_role/m_ap_role');
	}
	
	public function index()
	{
		if (in_array('read', $this->access)) {
			$data['access'] = $this->access;
			$data['title'] = 'Manajemen';
			$data['menu'] = $this->menu;
	
			// get session
			$data['search'] = $this->session->userdata('search');
			$data['order'] = $this->session->userdata('order');
			
			// count
			$data['num_rows'] = $this->m_dt_pengangguran->num_rows($data['search']);
			$data['num_rows_total'] = $this->m_dt_pengangguran->num_rows_total($data['search']);
			
			// pagination config
			$config['per_page'] = $this->session->userdata('per_page');
			if($config['per_page'] == null) {
				$config['per_page'] = 10;
				$this->session->set_userdata('per_page', $config['per_page']);
			};
			$config['total_rows'] = $data['num_rows'];
			$data['per_page'] = $config['per_page'];
			$config['base_url'] = base_url().$this->menu->controller."/index/";
			$this->pagination->initialize($config);
			
			// get from url
			$data['from'] = $this->uri->segment(3);
			if($data['from'] == '') $data['from'] = 0;
			$this->session->set_userdata('page', $data['from']);
			$data['page'] = $this->session->userdata('page');
	
			// data
			$query = $this->m_dt_pengangguran->get_list($config['per_page'],$data['from'],$data['search'],$data['order']);
			$data['main'] = $query->result();
			$data['main_row'] = $query->num_rows();
	
			$data['pagination_info'] = "Menampilkan ";
			if ($data['main'] == null) {
				$data['pagination_info'] .= '0 sampai 0 dari 0 data.';
			}else{
				if ($data['main_row'] > 0) {
					$data['pagination_info'] .= ($data['from']+1);
				}else{
					$data['pagination_info'] .= ($data['from']);
				}
				$data['pagination_info'] .= " sampai ".($data['from']+$data['main_row'])." dari ".$data['num_rows']." data.";
			}
	
			// create log
			create_log('read',$this->menu->menu);
			$this->render('index',$data);
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		if (in_array('read', $this->access)) {
			$search = $this->input->post(null,true);
			$this->session->set_userdata(array('search' => $search));
			redirect(base_url($this->menu->controller));
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function reset()
  {
		if (in_array('read', $this->access)) {
			$this->session->unset_userdata('search');
			$this->session->unset_userdata('order');
			$this->session->unset_userdata('per_page');
			redirect(base_url($this->menu->controller).'/index');
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function order($order_field, $order_type)
	{
		if (in_array('read', $this->access)) {
			$order = array(
				'order_field' => $order_field,
				'order_type' => $order_type
			);
			$this->session->set_userdata(array('order' => $order));
			redirect(base_url($this->menu->controller));
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function per_page($limit)
	{
		if (in_array('read', $this->access)) {
			$this->session->set_userdata(array('per_page' => $limit));
			redirect(base_url($this->menu->controller));
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['main_list'] = $this->m_dt_pengangguran->get_all();
		$data['access'] = $this->access;
		$data['menu'] = $this->menu;
		
		if ($id == null) {
			if (in_array('create', $this->access)) {
				$data['role_list'] = $this->m_ap_role->get_all();
				$data['title'] = 'Tambah';
				$data['action'] = 'create';
				$data['main'] = null;
				$this->render('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if (in_array('update', $this->access)) {
				$data['title'] = 'Ubah';
				$data['action'] = 'update';
				$data['main'] = $this->m_dt_pengangguran->get_by_id($id);
				$this->render('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function create()
	{
		if (in_array('create', $this->access)) {
			$data = $this->input->post(null,true);
			if ($data != null) {
				$cek = $this->m_dt_pengangguran->get_by_tahun($data['tahun']);
				if ($cek == null) {
					$data['id'] = md5(date('Y-m-d H:i:s'));
					$data['created'] = date('Y-m-d H:i:s');
					$data['created_by'] = $this->session->userdata('fullname');
					if(!isset($data['is_active'])){$data['is_active'] = 0;}
					$this->m_dt_pengangguran->create($data);
					create_log('create',$this->menu->menu);
					$this->session->set_flashdata('flash', 'Data berhasil ditambahkan.');
	
					//redirect
					$num_rows = $this->m_dt_pengangguran->num_rows();
					$per_page = $this->session->userdata('per_page');
					$num_page = intval($num_rows/$per_page);
					if ($num_page == 0) {
						$page = 0;
					}else{
						$mod = $num_rows % $per_page;
						if ($mod == 0) {
							$page = $num_rows - $per_page;
						}else{
							$page = $num_rows - $mod;
						}
					}
					redirect(base_url($this->menu->controller).'/index/'.$page);
				}else{
					$this->session->set_flashdata('flash', 'Tahun sudah ada di sistem!');
					redirect(base_url().'dt_pengangguran/form');
				}
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		if(in_array('update', $this->access)){
			$data = $this->input->post(null,true);
			if ($data != null) {
				$data['updated'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('fullname');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				if($data['tahun_old'] == $data['tahun']){
					$id = $data['id'];
					unset($data['tahun_old']);
					$this->m_dt_pengangguran->update($id, $data);
					create_log('update',$this->menu->menu);
					$this->session->set_flashdata('flash', 'Data berhasil diubah.');

					//redirect
					$page = $this->session->userdata('page');
					redirect(base_url($this->menu->controller).'/index/'.$page);
				}else{
					$cek = $this->m_dt_pengangguran->get_by_tahun($data['tahun']);
					if ($cek == null) {
						$id = $data['id'];
						unset($data['tahun_old']);
						$this->m_dt_pengangguran->update($id, $data);
						create_log('update',$this->menu->menu);
						$this->session->set_flashdata('flash', 'Data berhasil diubah!');

						//redirect
						$page = $this->session->userdata('page');
						redirect(base_url($this->menu->controller).'/index/'.$page);
					}else{
						$this->session->set_flashdata('flash', 'Tahun sudah ada di sistem!');
						redirect(base_url($this->menu->controller).'/form/'.$data['id']);
					}
				}
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete($id = null)
	{
		if (in_array('delete', $this->access)) {
			if($id != null){
				$this->m_dt_pengangguran->delete_permanent($id);
				create_log('delete',$this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil dihapus!');
				redirect(base_url($this->menu->controller).'/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function multiple($type = null)
	{
		$data = $this->input->post(null,true);
		if (in_array('update', $this->access) || in_array('delete', $this->access)) {	
			if(isset($data['checkitem'])){
				foreach ($data['checkitem'] as $key) {
					switch ($type) {					
						case 'delete':
							$this->m_dt_pengangguran->delete_permanent($key);
							$flash = 'Data berhasil dihapus.';
							$page = '';
							break;
	
						case 'enable':
							$this->m_dt_pengangguran->enable($key);
							$flash = 'Data berhasil diaktifkan.';
							$page = $this->session->userdata('page');
							break;
	
						case 'disable':
							$this->m_dt_pengangguran->disable($key);
							$flash = 'Data berhasil dinonaktifkan.';
							$page = $this->session->userdata('page');
							break;
					}
				}
				create_log($type, $this->menu->menu);
			}
			$this->session->set_flashdata('flash', $flash);
			//redirect
			redirect(base_url($this->menu->controller).'/index/'.$page);
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function enable($id = null)
	{
		if (in_array('update',$this->access)) {
			$this->m_dt_pengangguran->enable($id);
			$page = $this->session->userdata('page');
			redirect(base_url($this->menu->controller).'/index/'.$page);
		}
	}

	public function disable($id = null)
	{
		if (in_array('update',$this->access)) {
			$this->m_dt_pengangguran->disable($id);
			$page = $this->session->userdata('page');
			redirect(base_url($this->menu->controller).'/index/'.$page);
		}
	}

}