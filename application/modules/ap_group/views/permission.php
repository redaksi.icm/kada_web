<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i> <?=@$title?> <?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Pengaturan
        </li>
        <li class="active">
          Autentikasi
        </li>
        <li class="active">
          <?=$menu->menu?>
        </li>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Daftar <?=@$menu->menu?></h3>
      </div>
      <form id="form" class="form-horizontal" action="<?=base_url($menu->controller)?>/permission_action" method="post">
        <div class="panel-body">
          <fieldset>
            <input type="hidden" name="id" id="id" value="<?=@$main->id?>">
            <div class="form-group">
              <label class="col-lg-2 control-label">Grup <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" value="<?=@$main->description?>" required readonly>
              </div>
            </div>  
          </fieldset>
          <div class="row">
            <label class="col-lg-2 control-label">Menu Permission <span class="text-danger">*</span></label>
            <div class="col-md-10 table-responsive" style="max-height:50vh;">
              
              <table class="table table-striped table-bordered table-condensed">
                <tbody>
                  <?php foreach($menu_list as $lvl1): ?>
                    <tr>
                      <td class="text-center" width="10">
                        <label class="checkall-label form-checkbox form-icon form-primary form-text" style="margin:5px">
                          <input class="checkall" type="checkbox" name="menu[]" value="<?=str_replace('.','-',$lvl1['id']);?>" <?php if($lvl1['group_id'] != null){echo 'checked';}?>>
                        </label>
                      </td>
                      <td colspan="99"><?=$lvl1['menu']?></td>
                    </tr>
                    <?php if(count($lvl1['children']) == 0): ?>
                      <?php foreach($lvl1['role'] as $role): ?>
                        <tr>
                          <td></td>
                          <td class="text-center" width="10">
                            <label class="checkall-label form-checkbox form-icon form-warning form-text" style="margin:5px">
                              <input class="checkall" type="checkbox" name="<?=str_replace('.','-',$lvl1['id']);?>[]" value="<?=$role['role']?>" <?php if($role['group_id'] != null){echo 'checked';}?>>
                            </label>
                          </td>
                          <td colspan="99"><?=$role['description']?></td>
                        </tr>
                      <?php endforeach;?>
                    <?php else: ?>
                      <?php foreach($lvl1['children'] as $lvl2): ?>
                        <tr>
                          <td></td>
                          <td class="text-center" width="10">
                            <label class="checkall-label form-checkbox form-icon form-primary form-text" style="margin:5px">
                              <input class="checkall" type="checkbox" name="menu[]" value="<?=str_replace('.','-',$lvl2['id']);?>" <?php if($lvl2['group_id'] != null){echo 'checked';}?>>
                            </label>
                          </td>
                          <td colspan="99"><?=$lvl2['menu']?></td>
                        </tr>
                        <?php if(count($lvl2['children']) == 0): ?>
                          <?php foreach($lvl2['role'] as $role): ?>
                            <tr>
                              <td></td>
                              <td></td>
                              <td class="text-center" width="10">
                                <label class="checkall-label form-checkbox form-icon form-warning form-text" style="margin:5px">
                                  <input class="checkall" type="checkbox" name="<?=str_replace('.','-',$lvl2['id']);?>[]" value="<?=$role['role']?>" <?php if($role['group_id'] != null){echo 'checked';}?>>
                                </label>
                              </td>
                              <td colspan="99"><?=$role['description']?></td>
                            </tr>
                          <?php endforeach;?>
                        <?php else: ?>
                          <?php foreach($lvl2['children'] as $lvl3): ?>
                            <tr>
                              <td></td>
                              <td></td>
                              <td class="text-center" width="10">
                                <label class="checkall-label form-checkbox form-icon form-primary form-text" style="margin:5px">
                                  <input class="checkall" type="checkbox" name="menu[]" value="<?=str_replace('.','-',$lvl3['id']);?>" <?php if($lvl3['group_id'] != null){echo 'checked';}?>>  
                                </label>
                              </td>
                              <td colspan="99"><?=$lvl3['menu']?></td>
                            </tr>
                            <?php if(count($lvl3['children']) == 0): ?>
                              <?php foreach($lvl3['role'] as $role): ?>
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td class="text-center" width="10">
                                    <label class="checkall-label form-checkbox form-icon form-warning form-text" style="margin:5px">
                                      <input class="checkall" type="checkbox" name="<?=str_replace('.','-',$lvl3['id']);?>[]" value="<?=$role['role']?>" <?php if($role['group_id'] != null){echo 'checked';}?>>
                                    </label>
                                  </td>
                                  <td colspan="99"><?=$role['description']?></td>
                                </tr>
                              <?php endforeach;?>
                            <?php endif; ?>
                          <?php endforeach;?>
                        <?php endif;?>
                      <?php endforeach;?>
                    <?php endif;?>
                  <?php endforeach;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-sm-7 col-sm-offset-2">
              <button class="btn btn-sm btn-primary btn-action" type="submit"><i class="fa fa-save"></i> Simpan</button>
              <a class="btn btn-sm btn-flat btn-default" href="<?=base_url($menu->controller)?>/index"><i class="fa fa-close"></i> Batal</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--End page content-->
</div>
<!--END CONTENT CONTAINER-->
<script>
  var faIcon = {
    valid: 'fa fa-check-circle fa-lg text-success',
    invalid: 'fa fa-times-circle fa-lg',
    validating: 'fa fa-refresh'
  }
  $('#form').bootstrapValidator({
    live: 'enabled',
		message: 'Nilai tidak valid.',
		feedbackIcons: faIcon,
		fields: {
     
    }
	}).on('success.field.bv', function(e, data) {
		var $parent = data.element.parents('.form-group');
		$parent.removeClass('has-success');
	}).on('success.form.bv', function(e) {
    $('.btn-action').html('<i class="fa fa-spin fa-spinner"></i> Proses...');
  });
</script>