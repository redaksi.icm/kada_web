<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_group extends CI_Model {

	public function get_list($number,$offset,$search = null, $order = null)
  {
    $where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.`group` LIKE '%".$search['term']."%' ";
        $where .= "OR a.description LIKE '%".$search['term']."%' ";
      }
    }

    if ($order == null) {
      $order['order_field'] = 'created';
      $order['order_type'] = 'asc';
      $this->session->set_userdata(array('order' => $order));
    }
    
    return $this->db->query(
      "SELECT * FROM ap_group a 
      $where
      ORDER BY "
        .$order['order_field']." ".$order['order_type'].
      " LIMIT 
        $offset, $number"
    );
  }

  function num_rows($search = null){
		$where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.`group` LIKE '%".$search['term']."%' ";
        $where .= "OR a.description LIKE '%".$search['term']."%' ";
      }
    }
    
    return $this->db->query(
      "SELECT * FROM ap_group a 
      $where"
    )->num_rows();
  }
  
  function num_rows_total($search = null){
    return $this->db->query(
      "SELECT * FROM ap_group a 
      WHERE is_deleted = 0"
    )->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted',0)
      ->where('is_active',1)
      ->order_by('created','asc')
			->get('ap_group')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('ap_group')->row();
  }

  public function get_group_menu($id)
  { 

    $query = $this->db->query(
      "SELECT 
        a.id, a.parent as parent_id, a.icon, a.type, a.menu, 
        b.group_id FROM ap_menu a
      LEFT JOIN ap_group_menu b ON b.menu_id = a.id AND b.group_id = '$id'
      ORDER BY a.id"
    );

    $res = array();
    foreach ($query->result_array() as $row)
    {
        $res[$row['id']] = $row;
        $res[$row['id']]['role'] = $this->get_menu_role($row['id'], $id);
    }

    return build_tree($res);
  }

  public function get_menu_role($menu_id,$group_id)
  {
    return $this->db->query(
      "SELECT a.menu_id, 
        b.role, b.description,
        c.group_id
      FROM ap_menu_role a
      JOIN ap_role b ON a.role_id = b.id
      LEFT JOIN ap_group_role c ON c.role = b.role AND c.menu_id = a.menu_id AND c.group_id = '$group_id'
      WHERE 
        a.menu_id = '$menu_id' AND b.role != 'read'
      ORDER BY b.created ASC"
    )->result_array();
  }

  public function permission_action($data)
  {
    echo '<pre>' . var_export($data, true) . '</pre>';
    if (isset($data['menu'])) {
      //delete into group menu
      $this->db->where('group_id', $data['id'])->delete('ap_group_menu');
      foreach ($data['menu'] as $menu) {
        //insert into group menu
        $d_menu = array(
          'group_id' => $data['id'],
          'menu_id' => str_replace('-','.',$menu),
          'created' => $data['created'],
          'created_by' => $data['created_by']
        );
        $this->db->insert('ap_group_menu', $d_menu);
        //delete group role and menu
        $this->db
          ->where('group_id', $data['id'])
          ->where('menu_id', str_replace('-','.',$menu))
          ->delete('ap_group_role');
        //insert read 
        $d_read = array(
          'group_id' => $data['id'],
          'menu_id' => str_replace('-','.',$menu),
          'role' => 'read',
          'created' => $data['created'],
          'created_by' => $data['created_by']
        );
        $this->db->insert('ap_group_role',$d_read);
        //check role
        if (isset($data[$menu])) {
          foreach ($data[$menu] as $role) {
            $d_role = array(
              'group_id' => $data['id'],
              'menu_id' => str_replace('-','.',$menu),
              'role' => $role,
              'created' => $data['created'],
              'created_by' => $data['created_by']
            );
            $this->db->insert('ap_group_role',$d_role);
          }
        }
      }
    }
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('ap_group')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('ap_group')->row();
  }

  public function create($data)
  {
    $this->db->insert('ap_group',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('ap_group',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id',$id)->update('ap_group',array('is_deleted' => 1));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('ap_group');
    $this->db->where('group_id',$id)->delete('ap_group_menu');
    $this->db->where('group_id',$id)->delete('ap_group_role');
  }

  public function enable($id)
  {
    $this->db->where('id',$id)->update('ap_group',array('is_active' => 1));
  }

  public function disable($id)
  {
    $this->db->where('id',$id)->update('ap_group',array('is_active' => 0));
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_group");
  }

}
