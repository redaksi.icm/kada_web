<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_profile extends MY_Controller {

	var $access, $menu, $group_id, $page;

  function __construct(){
		parent::__construct();

		$controller = 'ap_profile';

    if($this->session->userdata('controller') != $controller){
      $this->session->unset_userdata('search');
      $this->session->unset_userdata('order');
      $this->session->unset_userdata('per_page');
      $this->session->set_userdata(array('controller' => $controller));
		}
    $this->load->model('ap_config/m_ap_config');
		$this->group_id = $this->session->userdata('group_id');
		$this->menu = $this->m_ap_config->get_menu($this->group_id,$controller);
		$this->access = $this->m_ap_config->get_access($this->group_id,$controller);
		
		if ($this->menu == null) {
			redirect(base_url().'ap_error/error_403');
		}

		$this->load->model('m_ap_profile');
	}
	
	public function index()
	{
		if (in_array('read', $this->access)) {
			$data['access'] = $this->access;
			$data['title'] = 'Manajemen';
			$data['menu'] = $this->menu;
			$data['main'] = $this->m_ap_profile->get_first();
			$data['action'] = 'update';
			// create log
			create_log('read',$this->menu->menu);
			$this->render('index',$data);
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function reset()
  {
		if (in_array('read', $this->access)) {
			$this->session->unset_userdata('search');
			$this->session->unset_userdata('order');
			$this->session->unset_userdata('per_page');
			redirect(base_url($this->menu->controller).'/index');
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		if(in_array('update', $this->access)){
			$data = $this->input->post(null,true);
			if ($data != null) {
				$data['updated'] = date('Y-m-d H:i:s');
				$data['updated_by'] = $this->session->userdata('fullname');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id'];				
				if (!empty($_FILES["image"]["name"])) {
					$data['logo'] = $this->_uploadImage();
				} else {
					$data['logo'] = $data["old_image"];
				}
				unset($data['image'],$data['old_image']);
				$this->m_ap_profile->update($id, $data);
				create_log('update',$this->menu->menu);
				$this->session->set_flashdata('flash', 'Data berhasil diubah.');
				//redirect
				$page = $this->session->userdata('page');
				redirect(base_url($this->menu->controller).'/index/'.$page);
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	private function _uploadImage()
	{
		$config['upload_path']          = './img/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['file_name']            = 'logo';
		$config['overwrite']						= true;
		$config['max_size']             = 1024;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			return $this->upload->data("file_name");
		}else{
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
			exit();
		}
		
		return "no-image.jpeg";
	}

}