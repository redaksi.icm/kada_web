<!--CONTENT CONTAINER-->
<div id="content-container">
  <!--Page Title-->
  <div class="pageheader">
    <h3><i class="fa fa-<?=@$menu->icon?>"></i> <?=@$title?> <?=@$menu->menu?> </h3>
    <div class="breadcrumb-wrapper">
      <ol class="breadcrumb">
        <li class="active">
          Pengaturan
        </li>
        <li class="active">
          Aplikasi
        </li>
        <li class="active">
          <?=$menu->menu?>
        </li>
        <li class="active">
          <?php if($action == 'create'):?>
            Tambah
          <?php else: ?>
            Ubah
          <?php endif; ?>
        </li>
      </ol>
    </div>
  </div>
  <!--End page title-->
  <!--Page content-->
  <div id="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Form <?=@$menu->menu?></h3>
      </div>
      <form id="form" class="form-horizontal" enctype="multipart/form-data" action="<?=base_url().$menu->controller?>/<?=$action?>" method="post" autocomplete="off">
        <input type="hidden" name="id" value="<?=@$main->id?>">
        <div class="panel-body">
          <div class="flash-data" data-flashdata="<?=$this->session->flashdata('flash')?>"></div>
          <fieldset>
            <h5>Aplikasi</h5>
            <div class="form-group">
              <label class="col-lg-2 control-label">Nama Aplikasi <span class="text-danger">*</span></label>
              <div class="col-lg-5">
                <input type="text" class="form-control input-sm" name="aplikasi" id="aplikasi" value="<?=@$main->aplikasi?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Singkatan <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="singkatan" id="singkatan" value="<?=@$main->singkatan?>" required>
              </div>
            </div>
            <h5>OPD</h5>
            <div class="form-group">
              <label class="col-lg-2 control-label">OPD <span class="text-danger">*</span></label>
              <div class="col-lg-4">
                <input type="text" class="form-control input-sm" name="opd" id="opd" value="<?=@$main->opd?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Provinsi <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="provinsi" id="provinsi" value="<?=@$main->provinsi?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Pemda <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <select id="pemda" name="pemda" class="form-control selectpicker">
                  <option value="Kabupaten" <?php if(@$main->pemda == 'Kabupaten'){echo 'selected';}?>>Kabupaten</option>
                  <option value="Kota" <?php if(@$main->pemda == 'Kota'){echo 'selected';}?>>Kota</option>
                </select>
              </div>
              <div class="col-lg-3">
                <input type="text" class="form-control input-sm" name="kab_kota" id="kab_kota" value="<?=@$main->kab_kota?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Kecamatan <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="kecamatan" id="kecamatan" value="<?=@$main->kecamatan?>" required>
              </div>
            </div>
            <h5>Alamat</h5>
            <div class="form-group">
              <label class="col-lg-2 control-label">Alamat <span class="text-danger">*</span></label>
              <div class="col-lg-5">
                <input type="text" class="form-control input-sm" name="alamat" id="alamat" value="<?=@$main->alamat?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Kode Pos <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="kodepos" id="kodepos" value="<?=@$main->kodepos?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Telepon <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="telp" id="telp" value="<?=@$main->telp?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Fax <span class="text-danger">*</span></label>
              <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="fax" id="fax" value="<?=@$main->fax?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Surel <span class="text-danger">*</span></label>
              <div class="col-lg-3">
                <input type="email" class="form-control input-sm" name="email" id="email" value="<?=@$main->email?>" required>
              </div>
            </div>
            <h5>Logo</h5>
            <div class="form-group">
              <label class="col-lg-2 control-label">Logo</label>
              <div class="col-lg-3">
                <input type="file" class="input-sm" name="image" id="image">
                <input type="hidden" class="input-sm" name="old_image" id="old_image" value="<?=@$main->logo?>">
              </div>
            </div>
            <div class="col-lg-3 col-lg-offset-2">
              <img src="<?=base_url()?>img/<?php if($main->logo == ''){echo 'no-image.jpeg';}else{echo $main->logo;}?>" width="100" alt="<?=$main->logo?>" class="img-responsive thumbnail">
            </div>
          </fieldset>
        </div>
        <div class="panel-footer">
          <div class="row">
            <div class="col-sm-7 col-sm-offset-2">
              <button class="btn btn-sm btn-primary btn-action" type="submit"><i class="fa fa-save"></i> Simpan</button>
              <a class="btn btn-sm btn-flat btn-default" href="<?=base_url($menu->controller)?>/index"><i class="fa fa-close"></i> Batal</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- End Page content -->
</div>
<!--END CONTENT CONTAINER-->
<script>
  var faIcon = {
    valid: 'fa fa-check-circle fa-lg text-success',
    invalid: 'fa fa-times-circle fa-lg',
    validating: 'fa fa-refresh'
  }
  $('#form').bootstrapValidator({
    live: 'enabled',
		message: 'Nilai tidak valid.',
		feedbackIcons: faIcon,
		fields: {
      kodepos: {
        validators: {
          digits : {}
        }
      },
      email: {
        validators: {
          email : {}
        }
      }
    }
	}).on('success.field.bv', function(e, data) {
		// $(e.target)  --> The field element
		// data.bv      --> The BootstrapValidator instance
		// data.field   --> The field name
		// data.element --> The field element

		var $parent = data.element.parents('.form-group');

		// Remove the has-success class
		$parent.removeClass('has-success');
	}).on('success.form.bv', function(e) {
    // Make button action proses
    $('.btn-action').html('<i class="fa fa-spin fa-spinner"></i> Proses...');
  });

  //chained
  $("#kab_kota_kd").chained("#provinsi_kd");
  $("#unit_kd").chained("#bidang_kd");
  $("#sub_unit_kd").chained("#unit_kd");
  $("#upb_kd").chained("#sub_unit_kd");
</script>