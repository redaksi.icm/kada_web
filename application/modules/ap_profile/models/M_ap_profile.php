<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_profile extends CI_Model {

	public function get_list($number,$offset,$search = null, $order = null)
  {
    $where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.role LIKE '%".$search['term']."%' ";
        $where .= "OR a.description LIKE '%".$search['term']."%' ";
      }
    }

    if ($order == null) {
      $order['order_field'] = 'created';
      $order['order_type'] = 'asc';
      $this->session->set_userdata(array('order' => $order));
    }
    
    return $this->db->query(
      "SELECT * FROM ap_profile a 
      $where
      ORDER BY "
        .$order['order_field']." ".$order['order_type'].
      " LIMIT 
        $offset, $number"
    );
  }

  function num_rows($search = null){
		$where = "WHERE is_deleted = 0 ";
    if ($search != null) {
      if ($search['term'] != '') {
        $where .= "AND a.role LIKE '%".$search['term']."%' ";
        $where .= "OR a.description LIKE '%".$search['term']."%' ";
      }
    }
    
    return $this->db->query(
      "SELECT * FROM ap_profile a 
      $where"
    )->num_rows();
  }
  
  function num_rows_total($search = null){
    return $this->db->query(
      "SELECT * FROM ap_profile a 
      WHERE is_deleted = 0"
    )->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted',0)
      ->where('is_active',1)
      ->order_by('created','asc')
			->get('ap_profile')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('ap_profile')->row();
  }

  public function get_kecamatan()
  {
    return $this->db->query(
      "SELECT * FROM opd_unit WHERE provinsi_kd=11 AND kab_kota_kd=13 AND bidang_kd=24"
    )->result();
  }

  public function get_first()
  {
    return $this->db->get('ap_profile')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('ap_profile')->row();
  }

  public function create($data)
  {
    $this->db->insert('ap_profile',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('ap_profile',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id',$id)->update('ap_profile',array('is_deleted' => 1));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('ap_profile');
  }

  public function enable($id)
  {
    $this->db->where('id',$id)->update('ap_profile',array('is_active' => 1));
  }

  public function disable($id)
  {
    $this->db->where('id',$id)->update('ap_profile',array('is_active' => 0));
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_profile");
  }

}
