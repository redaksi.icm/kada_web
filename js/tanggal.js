function showTime() {
  var waktu = new Date();
  var sh = waktu.getHours() + "";
  var sm = waktu.getMinutes() + "";
  var ss = waktu.getSeconds() + "";
  document.getElementById("showTimeOne").innerHTML = '<i class="fa fa-clock-o"></i> ' +(sh.length == 1 ? "0" + sh : sh) + ":" + (sm.length == 1 ? "0" + sm : sm) + ":" + (ss.length == 1 ? "0" + ss : ss);  
}

function showDate() {
  var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
  var date = new Date();
  var day = date.getDate();
  var month = date.getMonth();
  var thisDay = date.getDay(),
    thisDay = myDays[thisDay];
  var yy = date.getYear();
  var year = (yy < 1000) ? yy + 1900 : yy;
  document.getElementById('showDayOne').innerHTML = '<i class="fa fa-calendar"></i> '+thisDay + ', ';
  document.getElementById('showDateOne').innerHTML = day + ' ' + months[month] + ' ' + year;
}

setInterval(showTime, 1000);
setInterval(showDate, 1000);